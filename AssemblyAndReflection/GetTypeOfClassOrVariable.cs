﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyAndReflection
{
    class GetTypeOfClassOrVariable
    {
        public static void Run()
        {
            //Get type of integer
            Type t = typeof(int);
            Console.WriteLine(t.Name); //print int32

            //Get type of variable
            string a = "abc";
            Console.WriteLine(a.GetType()); //print System.String
        }
    }

}
