﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyAndReflection
{
    class Employee
    {
        int _id;
        string _name;

        public Employee(int _id, string _name)
        {
            this._id = _id;
            this._name = _name;
        }

        public Employee()
        {

        }

        public String GetFullName()
        {
            return this._name;
        }
    }

    class ClassMethodInfo
    {
        public static void Run()
        {
            Console.WriteLine("----------------Constructor info--------------");
            Type t = typeof(Employee);
            //Get constructor of class Employee
            ConstructorInfo[] constructorInfos = t.GetConstructors();
            
            //Get information about constructor
            foreach(ConstructorInfo constructorInfo in constructorInfos)
            {
                Console.WriteLine("Is Constructor? : {0}", constructorInfo.IsConstructor);
                Console.WriteLine("Name: {0}",constructorInfo.Name);
                //Get number of parameters in class.
                Console.WriteLine("Number of params: {0}", constructorInfo.GetParameters().Length);
                Console.WriteLine();
            }

            //
            Console.WriteLine("----------------Methods info--------------");
            MethodInfo[] methodInfos = t.GetMethods();

            foreach(MethodInfo methodInfo in methodInfos)
            {
                Console.WriteLine("Method name: {0}", methodInfo.Name);
                Console.WriteLine("Method return type : {0}", methodInfo.ReturnType);
                Console.WriteLine("Method is public? : {0}", methodInfo.IsPublic);
                Console.WriteLine();
            }
        }
    }
}
