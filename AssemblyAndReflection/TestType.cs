﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyAndReflection
{
    public class Customer
    {
        public string myId;
        public Customer()
        {

        }
        public Customer(int id, string name, int age)
        {
            Id = id;
            Name = name;
            Age = age;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
    }

    class TestType
    {
        public static void Run()
        {
            Type t = typeof(Customer);
            ConstructorInfo[] constructorInfos = t.GetConstructors();
            foreach (ConstructorInfo c in constructorInfos)
            {
                Console.WriteLine(c.GetParameters().Length);
                Console.WriteLine(c.Name);
                
            }
            Console.WriteLine("===========================");
            MethodInfo[] methods = t.GetMethods();
            foreach (MethodInfo method in methods)
            {
                Console.WriteLine(method.Name);
                Console.WriteLine(method.GetParameters().Length);
                Console.WriteLine(method.IsPublic);
            }

            Console.WriteLine("===========================");

            PropertyInfo[] propertyInfos = t.GetProperties();
            foreach(PropertyInfo propertyInfo in propertyInfos)
            {
                Console.WriteLine("Property:{0}", propertyInfo.Name);
            }

            Console.WriteLine("==========Field===============");
            FieldInfo[] fieldInfos = t.GetFields();
            foreach (FieldInfo fieldInfo in fieldInfos)
            {
                Console.WriteLine("Field:{0}", fieldInfo.Name);
            }
        }
    }
}
