﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AssemblyAndReflection
{
    class MyResourceManager
    {
        public static void Run()
        {
            CultureInfo cultureInfo = new CultureInfo("es-ES");
            Thread.CurrentThread.CurrentCulture = cultureInfo;
            Thread.CurrentThread.CurrentUICulture = cultureInfo;
            ResourceManager myResourceManager = new ResourceManager("AssemblyAndReflection.MyResource",Assembly.GetExecutingAssembly());
            Console.WriteLine(myResourceManager.GetString("Name"));
            Console.WriteLine(myResourceManager.GetString("User"), "camnh");
        }
    }
}
