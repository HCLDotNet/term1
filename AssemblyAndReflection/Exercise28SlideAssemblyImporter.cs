﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyAndReflection
{
    class Exercise28SlideAssemblyImporter
    {
        public static void Run()
        {
            Console.WriteLine(SampleLibrary.Exercise28SlideAssembly.ConvertRoman(300));
            Console.WriteLine(SampleLibrary.Exercise28SlideAssembly.ConvertWords(300));
        }
    }
}
