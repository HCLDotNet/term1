﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyAndReflection
{
    class GetInfoOfExistingAssembly
    {
        public static void Run()
        {
            Assembly assembly = Assembly.LoadFrom(@"C:\Users\Administrator\Desktop\Term1\SampleLibraryToUseInAssembly\bin\Debug\SampleLibrary.dll");
            //Remember to use full namespace.Class to point to exact dll
            Type type = assembly.GetType("SampleLibrary.SampleDLLLibrary");

            //create instance of class but without using new
            var obj = Activator.CreateInstance(type);
            //invoke method of class, can pass param if we want
            MethodInfo add = type.GetMethod("Add");
            //calling method inside dll, passing no parameter array
            int resultOfAdd = (int)add.Invoke(obj, new object[] { });

            //Here we can use reflection to get name of class, etc
            Console.WriteLine("Class: {0}", type.Name);
            Console.WriteLine("Result of add is: {0}", resultOfAdd);

        }
    }
}
