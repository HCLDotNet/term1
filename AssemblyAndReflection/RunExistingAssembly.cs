﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyAndReflection
{
    class RunExistingAssembly
    {
        public static void Run()
        {           
            AppDomain appDomain = AppDomain.CreateDomain("New domain");          
            //Type type = assemblyInfo.GetType();
            //MethodInfo[] methods = type.GetMethods();
            //Run assembly exe
            appDomain.ExecuteAssembly(@"D:\Term1.exe");
        }
    }
}
