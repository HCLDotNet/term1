﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyAndReflection.InClass
{
    public class Employee
    {
        public int id;
        string name;

        public Employee()
        {

        }

        public Employee(int id, string name)
        {
            this.id = id;
            this.name = name;
        }

        public string printEmployee()
        {
            return this.id + "----" + this.name;
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class TypeReflection
    {
        
        //Working with files:
        //if I want to list files inside directory

       public static void Run()
        {
            Type empType = typeof(Employee);
            ConstructorInfo[] constructorInfos = empType.GetConstructors();
            foreach (ConstructorInfo constructorInfo in constructorInfos)
            {
                Console.WriteLine("Ctor: {0} - Parameter: {1}",constructorInfo.Name, constructorInfo.GetParameters().Length);
                ParameterInfo[] pI = constructorInfo.GetParameters();
                foreach (ParameterInfo pItem in pI) {
                    Console.WriteLine(pItem.Name);
                }
            }

            Console.WriteLine("====================Methods==================");
            MethodInfo[] methodInfos = empType.GetMethods();
            foreach (MethodInfo methodInfo in methodInfos)
            {
                Console.WriteLine(methodInfo.Name);
                Console.WriteLine(methodInfo.IsPublic);
                Console.WriteLine(methodInfo.IsPrivate);
            }

            Console.WriteLine("====================Properties==================");
            PropertyInfo[] propertiesInfos = empType.GetProperties();
            foreach(PropertyInfo propertyInfo in propertiesInfos)
            {
                Console.WriteLine(propertyInfo.Name);
                Console.WriteLine(propertyInfo.PropertyType);
            }
            Console.WriteLine("====================Fields=======================");
            FieldInfo[] fieldInfos = empType.GetFields();
            foreach (FieldInfo fieldInfo in fieldInfos) {
                Console.WriteLine(fieldInfo.Name);
            }
        }
    }
}
