﻿using AssemblyAndReflection.InClass;
using SampleLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace AssemblyAndReflection
{
    class Program
    {
        static void Main(string[] args)
        {
            //uncomment to get type of variable
            //GetTypeOfClassOrVariable.Run();

            //uncomment to get classinfo
            //ClassMethodInfo.Run();

            //uncomment to run existing assembly
            //RunExistingAssembly.Run();

            //uncomment to load information about assembly dll
            //GetInfoOfExistingAssembly.Run();

            //Class library is already created within the solution, named
            //SampleLibraryToUseInAssembly
            //Using methods inside sample library
            //Exercise28SlideAssemblyImporter.Run();

            //TestType.Run();

            //Console.WriteLine(Class1.ToLower("CAMNH"));

            //MyResourceManager.Run();

            //AppDomain appDomain = AppDomain.CreateDomain("my");
            //appDomain.Load(@"C:\Users\camnh\Desktop\Term1.exe");
            //appDomain.ExecuteAssembly(@"C:\Users\camnh\Desktop\Term1.exe");

            //Assembly ass = Assembly.LoadFile(@"C:\Users\camnh\Desktop\term1\AssemblyAndReflection\bin\Debug\SampleLib.dll");
            //Type t = ass.GetType("SampleLib.Class1");
            //var obj = Activator.CreateInstance(t);
            //Console.WriteLine(t.GetMethods()[0]);

            Assembly ass1 = Assembly.LoadFile(@"C:\Users\camnh\Desktop\term1\AssemblyAndReflection\bin\Debug\SampleLibrary.dll");
            Type t1 = ass1.GetType("SampleLibrary.SampleDLLLibrary");
             
            Console.WriteLine("Sample DLL has method");
            MethodInfo[] methodInfos = t1.GetMethods();
            foreach (MethodInfo methodInfo in methodInfos)
            {
                Console.WriteLine(methodInfo.Name);
            }

            
            Console.WriteLine();
            //TypeReflection.Run();
            //AppDomain myFriendlyDomain = AppDomain.CreateDomain("myAppFriendlyNameDomain");
            //myFriendlyDomain.ExecuteAssembly(@"C:\Users\camnh\Desktop\Term1.exe");
            Console.ReadLine();
        }
    }
}
