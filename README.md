# About term 1
Term 1 contains core principles from base C# includes: OOP, Inheritance, Polymorphism and some advancements such as Files, Delegates and Reflections. Topic of each is the name of folder itself.

# User guide

Each project folder will contain a file call `Program.cs` which contains `main()` method for execution. The project is wrapped in solution so that reader can open solution directly on Visual Studio

# Platform
IDE: Visual Studio 2017 Community Edition
