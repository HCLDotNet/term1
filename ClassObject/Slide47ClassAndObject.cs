﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassObject
{
    class Slide47ClassAndObject
    {
        
    }

    class Book
    {
        string _isbn;
        string _title;
        string _author;
        string _price;
        int _stock = 0;

        public Book(string _isbn, string _title, string _author, string _price)
        {
            this._isbn = _isbn;
            this._title = _title;
            this._author = _author;
            this._price = _price;            
        }

        public String ISBN { get { return this._isbn; } set { this._isbn = value; } }
    }
}
