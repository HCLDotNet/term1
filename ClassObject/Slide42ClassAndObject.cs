﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassObject
{
    class Slide42ClassAndObject
    {
        private static int RollNo;
        private string Name;
        private string Degree;
        private string Semester;

        public Slide42ClassAndObject(string name, string degree, string semester)
        {
            RollNo++;
            Name = name;
            Degree = degree;
            Semester = semester;
        }

        public void Display()
        {
            Console.WriteLine("{0} -- {1} -- {2} -- {3}",RollNo, Name, Degree, Semester);
        }
    }
}
