﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassObject
{
    class Slide99ClassAndObject
    {
        public static void CheckPalindrome (string a)
        {
            char[] ch = a.ToCharArray();
            bool isPalindrome = true;
            for (int i = 0; i<ch.Length/2; i++)
            {
                if (ch[i] != ch[ch.Length - 1 - i])
                {
                    isPalindrome = false;
                    break;
                }
            }

            if (isPalindrome)
            {
                Console.WriteLine("String is palindrome");
            }
            else
            {
                Console.WriteLine("String is not palindrome");
            }
        }
    }
}
