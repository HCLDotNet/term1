﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassObject
{
    class Slide45ClassAndObject
    {
    }

    class DiscoverIndia
    {
        string _packageName;
        string _placeOfVisit;
        double _cost;

        public string PackageName { get { return this._packageName; } }
        public string PlaceOfVisit { get { return this._placeOfVisit; } set { this._placeOfVisit = value; } }
        public double Cost { get { return this._cost; } set { this._cost = value; } }

        public DiscoverIndia(string packageName, string place, double cost)
        {
            _packageName = packageName;
            _placeOfVisit = place;
            _cost = cost;
        }
    }

    class HolidayHungama
    {
        string _packageName;
        string _placeOfVisit;
        double _cost;

        public string PackageName { get { return this._packageName; } }
        public string PlaceOfVisit { get { return this._placeOfVisit; } set { this._placeOfVisit = value; } }
        public double Cost { get { return this._cost; } set { this._cost = value; } }

        public HolidayHungama(string packageName, string place, double cost)
        {
            _packageName = packageName;
            _placeOfVisit = place;
            _cost = cost;
        }
    }

    class Pilgrimage
    {
        string _packageName;
        string _placeOfVisit;
        double _cost;

        public string PackageName { get { return this._packageName; } }
        public string PlaceOfVisit { get { return this._placeOfVisit; } set { this._placeOfVisit = value; } }
        public double Cost { get { return this._cost; } set { this._cost = value; } }

        public Pilgrimage(string packageName, string place, double cost)
        {
            _packageName = packageName;
            _placeOfVisit = place;
            _cost = cost;
        }
    }


    class HappyTravel
    {
        static DiscoverIndia DiscoverIndia;
        static HolidayHungama HolidayHungama;
        static Pilgrimage Pilgrimage;

        static HappyTravel()
        {
            DiscoverIndia = new DiscoverIndia("hello", "hn", 10.3);
            HolidayHungama = new HolidayHungama("hellohgm", "bk", 10.4);
            Pilgrimage = new Pilgrimage("hellop", "vc", 10.5);
        }

        public static void Display()
        {
            Console.WriteLine("Name:" + DiscoverIndia.PackageName);
            //...
        }
    }
}
