﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassObject
{
    class Employee
    {
        // scope is nothing ==> private
        int Id;
        string Name;

        // property with getter and setter
        public string Height { get; set; }
        // This property above is similar to
        // private string height;
        // public string GetHeight() { return height; }
        // public void SetHeight(string heightParameter) { height = heightParameter; }

        // using private Setter property
        public string privateSetterProperty {
            get { return privateSetterProperty; }
            // using value to set value for property.
            private set { privateSetterProperty = value; }
        }

        /**
         * Using constant in class
         */
        public const double PI = 3.14;

        public Employee(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public void PrintName()
        {
            // Concatenate using "+" sign
            Console.WriteLine("Name of employee is " + this.Name);
        }

        public void PrintHeight()
        {
            Console.WriteLine("Height of employee is: " + Height);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // Initialize instance of object
            Employee e = new Employee(1, "camnh");
            Employee e1 = new Employee(2, "congnv");

            e.PrintName();
            e1.PrintName();

            // using setter on Employee instance, set property of instance of class.
            e.Height = "185";
            e.PrintHeight();

            //Using constant ==> call property directly in Class.
            Console.WriteLine(Employee.PI);

            //From Slide42ClassAndObject
            Slide42ClassAndObject s1 = new Slide42ClassAndObject("camnh", "G", "1");
            s1.Display();
            Slide42ClassAndObject s2 = new Slide42ClassAndObject("camnh", "G", "1");
            s1.Display();

            //From Slide43ClassAndObject, reading const on App.Config
            //Remember to Import System.Configuration at the top
            //Remember to add key name; here PI is name of key in App.Config
            Console.WriteLine("PI from configuration: " + Convert.ToDouble(ConfigurationSettings.AppSettings["PI"]));

            //From Slide45ClassAndObject
            HappyTravel happyTravel = new HappyTravel();
            HappyTravel.Display();


            //From Slide47ClassAndObject
            Console.WriteLine("----------Book store----------");
            Book[] books = new Book[2];
            books[0] = new Book("1", "2", "3", "4");
            books[1] = new Book("2", "3", "4", "5");

            for (int i = 0; i < books.Length; i++)
            {
                Console.WriteLine(books[i].ISBN);
            }

            Console.WriteLine("--------------Palindrome------------");
            Slide99ClassAndObject.CheckPalindrome("madama");
            Console.ReadLine();
        }
    }
}
