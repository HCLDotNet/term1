﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExceptionHandling
{
    class NumberIsOddException : Exception
    {
        string s;

        //Using custom message provided by user
        public NumberIsOddException(string message = "")
        {
            s = message == "" ? "Number is odd" : message;
        }

        public override string ToString()
        {
            return s;
        }
    }

    class TestDefinedException
    {
        public void Test()
        {
            int a = 5;
            if (a % 2 != 0)
            {
                //force to log exception
                throw new NumberIsOddException("Number must be odd");
            }
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            #region Use BuiltIn Exception
            //built-in exception
            int dividend = 9;
            int divisor = 1;
            //catch exception using try catch
            try
            {
                Console.WriteLine(dividend / divisor);
            }
            //show full error message content
            //and error stack tracing
            //catch specific exception on page
            catch (DivideByZeroException e)
            {
                Console.WriteLine("Specific exception:" + e.Message);
                Console.WriteLine("Specific exception trace:" + e.StackTrace);
            }
            //or if you dont know exception detail
            //all errors are generalized to Exception class
            catch (Exception e)
            {
                Console.WriteLine(e.Message); // Show message
                Console.WriteLine(e.StackTrace);// Show stack trace where error occurs.
                Console.WriteLine(e.GetType()); // Get type of exception class
            }
            #endregion

            #region Using Custom Exception Line14
            //when throwing exception at class level
            //catch exception using try catch
            try
            {
                TestDefinedException testDefinedException = new TestDefinedException();            
                testDefinedException.Test();
            }
            catch (Exception e)
            {            
                //Print custom exception message
                Console.WriteLine("Message: " + e);
                //here you can throw exception again, useful when class hierachies is used
                //by calling "throw e"
                //parent class needs to handle "throw e" fired in child class.
            }
            //finally block is executed whether program runs successfully or not
            finally
            {
                Console.WriteLine("Custom Exception class executed");
            }

            #endregion
            Console.ReadLine();
        }
    }
}
