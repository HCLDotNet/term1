﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


//Exercise Slide 97 - Classes and objects
namespace StringExcercise
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = "The quick brown fox jumps over the lazy dogs";
            Console.WriteLine(s[12]);
            Console.WriteLine("String contains the word \"is\": " + s.Contains("is"));
            Console.WriteLine(s + " and killed it");
            Console.WriteLine(s.EndsWith("dogs"));
            Console.WriteLine(s == "The quick brown fox jumps over the lazy dogs");
            Console.WriteLine(s == "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOGS");
            Console.WriteLine(s.Count());
            //Match ??
            Console.WriteLine(s.Replace("The", "A"));
            //Split
            string[] animals = s.Split(new char[] { 'j' });

            Console.WriteLine("Two animals: {0}, {1}", animals[0], animals[1]);
            // to lower case
            Console.WriteLine("To lowercase: {0}", s.ToLower());
            // to upper case
            Console.WriteLine("To uppercase: {0}", s.ToUpper());
            // index position of 'a'
            Console.WriteLine("Index of character \'a\': " + s.IndexOf('a'));
            // index position of 'a'
            Console.WriteLine("Last index of character \'e\': " + s.LastIndexOf('e'));

            Console.ReadLine();
        }
    }
}
