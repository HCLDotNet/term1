﻿using System;
namespace GenericClass
{
    #region Exercise1
    public class Exercises<T>
    {
        T[] elements;

        public Exercises(T[] elem) {
            elements = elem;
        }

        public void Swap()
        {
            //[0,1]
            T temp = elements[0];
            elements[0] = elements[1];
            elements[1] = temp;
        }

        public void Print()
        {
            foreach (T item in elements)
            {
                Console.WriteLine(item);
            }
        }
    }
    #endregion

    #region Exercise2
    public class Exercise2<T> where T : IComparable
    {
        T[] elements;

        public Exercise2(T[] ele)
        {
            elements = ele;
        }

        public T GetMax(int start, int end)
        {
            T max = default(T);

            for (int i = start; i <= end; i++)
            {
                if (elements[i].CompareTo(max) > 0) {
                    max = elements[i];
                }
            }

            return max;
        }
    }
    #endregion

    #region Exercise3
    public class MySet<T>
    {
        T[] elements;
        int size;
        int currentIndex;

        public MySet(int size)
        {
            this.size = size;
            elements = new T[size];
        }

        public void Add(T item)
        {
            bool isDuplicated = false;

            if (currentIndex >= size)
            {
                foreach (T element in elements) {
                    if (element.Equals(item)) {
                        isDuplicated = true;
                        break;
                    }
                }

                if (!isDuplicated) {
                    this.size = size + 1;
                    Array.Resize(ref elements, this.size);     
                }
            }

            if (!isDuplicated)
            {
                elements[currentIndex++] = item;
            }
        }

        public void Remove(T item)
        {
            bool isFound = false;
            int newIndex = 0;

            for (int i = 0; i < this.size; i++) {
                if (elements[i].Equals(item))
                {
                    isFound = true;
                    break;
                }
            }

            if (isFound) {
                int originalSize = this.size;
                this.size = originalSize - 1;
                T[] arr = new T[this.size];
                for (int i = 0; i < originalSize; i++)
                {
                    if (!elements[i].Equals(item))
                    {
                        arr[newIndex++] = elements[i];                  
                    }
                }
                elements = arr;
            }
        }

        public bool Contains(T item)
        {
            foreach (T element in elements)
            {
                if (element.Equals(item))
                {
                    return true;
                }
            }

            return false;
        }

        public void Display()
        {
            foreach(T element in elements)
            {
                Console.WriteLine(element);
            }
        }

    }
    #endregion

    public class ExerciseRunner
    {
        public static void Run()
        {
            //Exercises<string> exercises = new Exercises<string>(new string[] {"a", "b"});
            //exercises.Swap();

            //Exercises<int> exDemo = new Exercises<int>(new int[] { 1,2 });
            //exDemo.Swap();

            //exercises.Print();
            //exDemo.Print();

            //Exercise2<int> exercise2 = new Exercise2<int>(new int[] { 1, 2, 3, 4, 5 });
            //Console.WriteLine(exercise2.GetMax(0, 3));

            //Exercise2<double> exercise21 = new Exercise2<double>(new double[] { 1.2, 2.3, 4.5 });
            //Console.WriteLine(exercise21.GetMax(0, 2));

            MySet<int> mySet = new MySet<int>(5);
            mySet.Add(1);
            mySet.Add(2);
            mySet.Add(3);
            mySet.Add(4);
            mySet.Add(5);
            mySet.Add(2);
            mySet.Add(9);

            mySet.Display();

            Console.WriteLine("-----------");
            mySet.Remove(1);
            mySet.Remove(2);
            mySet.Remove(4);
            Console.WriteLine("-----------");

            mySet.Display();
        }
    }
}
