﻿using System;
using System.Collections.Generic;

namespace GenericClass
{
    //TKey is struct type ?? Can I accept TKey string ??
    class GenericDictionary<TKey, TValue> where TKey:class,IComparable
    {
        KeyValuePair<TKey, TValue>[] keyValuePair;
        int size;
        int currIndex = 0;

        public GenericDictionary(int size)
        {
            this.size = size;
            this.keyValuePair = new KeyValuePair<TKey, TValue>[size];
        }

        public void Add(TKey key, TValue value)
        {
            if (key == null) {
                //1. throw new Exception()
                //2. key = new TKey();
            }
            foreach (KeyValuePair<TKey, TValue> pair in keyValuePair)
            {
                if (!pair.Equals(default(KeyValuePair<TKey, TValue>)))
                {
                    if (pair.Key.CompareTo(key) == 0)
                    {
                        throw new ArgumentException("Duplicate Keys");
                    }
                }
            }

            if (currIndex >= this.size)
            {
                throw new IndexOutOfRangeException();
            }
            keyValuePair[currIndex++] = new KeyValuePair<TKey, TValue>(key, value);
        }

        public void Display()
        {
            for (int i = 0; i < this.size; i++)
            {
                Console.WriteLine("Key: {0} Value: {1}", keyValuePair[i].Key, keyValuePair[i].Value);
            }
        }
    }

    public class GenericDictionaryRunner
    {
        public static void Run()
        {
            GenericDictionary<string, string> genericDictionary = new GenericDictionary<string, string>(3);
            genericDictionary.Add("1", "a");
            genericDictionary.Add("2", "b");
            genericDictionary.Add("1", "c");

            genericDictionary.Display();
        }
    }
}
