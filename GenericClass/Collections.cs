﻿using System;
using System.Collections.Generic;

namespace GenericClass
{
    public class Collections
    {
        public static void ListRunner()
        {
            List<int> ls = new List<int>();
            ls.Add(1);
            ls.Add(2);
            ls.Add(3);

            ls[1] = 5;

            foreach (int i in ls)
            {
                Console.WriteLine(i);
            }
        }

        public static void DictonaryRunner()
        {
            Dictionary<int, string> dict = new Dictionary<int, string>();
            dict.Add(1, "a");
            dict.Add(2, "b");
            dict.Add(3, "c");

            dict[2] = "d";

            foreach (KeyValuePair<int,string> keyValuePair in dict)
            {
                Console.WriteLine(keyValuePair.Key + "-----" + keyValuePair.Value);
            }
        }

        public static void StackRunner()
        {
            Stack<int> s = new Stack<int>();
            s.Push(1);
            s.Push(2);

            foreach (int i in s)
            {
                Console.WriteLine(i);
            }
        }

        public static void QueueRunner()
        {
            Queue<string> q = new Queue<string>();
            q.Enqueue("A");
            q.Enqueue("B");
            q.Enqueue("C");

            q.Dequeue();

            foreach (string i in q)
            {
                Console.WriteLine(i);
            }
        }

        public static void Run()
        {
            ListRunner();
            DictonaryRunner();
            StackRunner();
            QueueRunner();
        }
    }
}
