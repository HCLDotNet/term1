﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenericClass
{

    class Program
    {
        static void Main(string[] args)
        {
            //Generic.Run();

            //GenericDictionaryRunner.Run();

            //GenericArray<int> genericArray = new GenericArray<int>(5);
            //genericArray.Add(1);
            //genericArray.Add(2);
            //genericArray.Add(3);
            //genericArray.Add(4);
            //genericArray.Add(5);
            //genericArray.PrintItems();

            //GenericArray<string> genericArray1 = new GenericArray<string>(3);
            //genericArray1.Add("1");
            //genericArray1.Add("2");
            //genericArray1.Add("3");
            //genericArray1.Add("4");
            //genericArray1.PrintItems();      

            //ExerciseRunner.Run();

            //Collections.Run();

            CollectionExample.Run();

            Console.ReadLine();
        }
    }
}
