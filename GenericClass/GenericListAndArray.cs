﻿using System;
namespace GenericClass
{

    //Using letter T for representing generic type
    class MyGenericArray<T>
    {
        //imagine replace int[] as T[]
        T[] elements;

        public MyGenericArray(int size)
        {
            elements = new T[size];
        }

        public T GetItem(int index)
        {
            return elements[index];
        }

        public void SetItem(int index, T value)
        {
            elements[index] = value;
        }

        public void PrintItems()
        {
            foreach (T item in elements)
            {
                Console.WriteLine(item);
            }
        }
    }

    //Exercise Generic Slide 10
    class MyLinkedList<T>
    {
        T[] elements;

        public MyLinkedList(int size)
        {
            elements = new T[size];
        }

        public T GetFirst()
        {
            return elements[0];
        }

        public T GetLast()
        {
            return elements[elements.Length - 1];
        }

        public T GetAt(int index)
        {
            return elements[index];
        }

        public void InsertAfter(int index, T value)
        {
            if (index + 1 != elements.Length)
            {
                elements[index + 1] = value;
            }
        }

        public void InsertBefore(int index, T value)
        {
            if (elements.Length != 0)
            {
                elements[index - 1] = value;
            }
        }

        public T[] DeleteAfter(int index)
        {
            T[] newArr = new T[elements.Length];
            for (int i = 0; i < elements.Length; i++)
            {
                if (index + 1 < elements.Length && i != index + 1)
                {
                    newArr[i] = elements[i];
                }
            }
            return newArr;
        }

        public void DeleteBefore(int index)
        {
            //Similar
        }
    }

    class Generic
    {
        public static void Run()
        {
            //Since we use T to repsent generic, using datatype here is our preference
            MyGenericArray<string> arr = new MyGenericArray<string>(3);
            arr.SetItem(0, "camnh");
            arr.SetItem(1, "congnv");
            arr.SetItem(2, "thangnx");
            arr.PrintItems();

            //Using different datatype, our generic class can cope with this well.
            MyGenericArray<int> arrInt = new MyGenericArray<int>(3);
            arrInt.SetItem(0, 2);
            arrInt.SetItem(1, 3);
            arrInt.SetItem(2, 4);
            arrInt.PrintItems();
        }
    }
    
}
