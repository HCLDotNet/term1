﻿using System;
using System.Collections.Generic;

namespace GenericClass
{
    public class CollectionExample
    {
        //List
        public static void ListRunner()
        {    
            //declare value
            //int a = 4;
            //int b = 6;
            //int c = 7;

            //// collections
            //int[] arr = new int[3] {a,b,c};

            ////print element of array??
            //for(int i = 0; i < arr.Length; i++)
            //{
            //    Console.WriteLine(i);
            //}

            List<int> ls = new List<int>();
            //CRUD : Insert (finished), Retrieve(finished), Update, delete
            ls.Add(1); //index 0
            ls.Add(2); //index 1
            ls.Add(3); //index 2

            ls[1] = 7; //[1,7,3]

            Console.WriteLine("----------Before delete---------");

            foreach (int i in ls)
            {
                Console.WriteLine(i);
            }

            Console.WriteLine("----------After delete-----------");
            ls.Remove(1);
            foreach (int i in ls)
            {
                Console.WriteLine(i);
            }
        }

        //Dictionary
        public static void DictionaryRunner()
        {
            Dictionary<string, int> dict = new Dictionary<string, int>();
            //[
            //  1 => "a",
            //  2 => "b"
            //  4 => "c"
            //]
            // the quick brown fox jumps over the lazy dogs
            // The number of occurrences
            // [
            //      "t" => 1,
            //      "h" => 2,
            //      "q" => 3
            // ]

            dict.Add("t", 1);
            dict.Add("h", 2);
            dict.Add("q", 3);

            dict["t"] = 5;

            dict.Remove("h");

            foreach(KeyValuePair<string, int> keyValuePair in dict)
            {      
                Console.WriteLine("Key: {0} --- Value: {1}", keyValuePair.Key, keyValuePair.Value);
            }
        }

        //Stack
        public static void StackRunner()
        {
            Console.WriteLine("---------Stack part-----------");
            Stack<int> stk = new Stack<int>();
            stk.Push(1);
            stk.Push(2); //from the top: []
            stk.Push(3);
            //3
            //2
            //1
            stk.Pop();
            foreach (int item in stk)
            {
                Console.WriteLine(item);
            }
        }
        //Queue
        public static void QueueRunner()
        {
            //Principle: FIFO
            Console.WriteLine("--------------Queue Runner----------");
            Queue<int> qu = new Queue<int>();
            qu.Enqueue(1); //[4,3,2,1] => order of execution
            qu.Enqueue(2);   
            qu.Dequeue();
            foreach(int i in qu)
            {
                Console.WriteLine(i);
            }

            //
        }

        //HashSet
        public static void HashSetRunner()
        {
            Console.WriteLine("--------------Hashset Runner----------");
            HashSet<int> hashSet = new HashSet<int>();
            hashSet.Add(1);
            hashSet.Add(2);
            hashSet.Add(1);

            foreach (int i in hashSet)
            {
                Console.WriteLine(i);
            }
        }

        public static void Run()
        {
            //ListRunner();

            //DictionaryRunner();

            //StackRunner();\

            //QueueRunner();

            HashSetRunner();
        }
    }
}
