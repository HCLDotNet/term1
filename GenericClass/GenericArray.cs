﻿using System;
using System.Collections.Generic;

namespace GenericClass
{

    public class GenericArray<T>
    {
        T[] elements;
        int size;
        int currentIndex;

        public GenericArray(int size) {
            this.size = size;
            this.elements = new T[size];    
        }

        public void Add(T item)
        {
            //loose => 4items => 6items
            //check currentIndex with its size
            if (currentIndex >= size) {
                throw new IndexOutOfRangeException("Index is out of range");
            }
            elements[currentIndex++] = item;
        }

        public void PrintItems()
        {
            foreach (T item in this.elements)
            {
                Console.WriteLine(item);
            }
        }
    }
}
