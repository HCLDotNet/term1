﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Polymorphism
{
    public interface IConnection
    {
        void Connect();
    }

    public interface IExport
    {
        void Export();
    }

    //implements interface syntax is similar to extends
    class MysqlConnection : IConnection
    {
        public void Connect()
        {
            Console.WriteLine("Connect using mysql");
        }
    }

    //a class can implement multiple interface
    // each interface is separated by a comma.
    // if multiple interfaces specified, class must implements all methods
    // in those interface
    class MongoDBConnection : IConnection, IExport
    {
        public void Connect()
        {
            Console.WriteLine("Connect using mongodb");
        }

        public void Export()
        {
            Console.WriteLine("Exported");
        }
    }

    //inject interface as parameter of constructor
    //user can change connection at main() without modification
    class DemoConnection
    {
        IConnection Connection;

        //here use interface, if connection changed, we don't need
        //to rewrite constructor
        public DemoConnection(IConnection connection)
        {
            Connection = connection;
        }

        public void Display()
        {
            Connection.Connect();
            //rest of code omitted
            //if connection is used for further research: var conn = Connection.connect();
        }

    }

    class Program
    {
        static void Main(string[] args)
        {
            //connection specified can change at main
            IConnection mysqlConnection = new MysqlConnection();
            //IConnection mongodbConnection = new MongoDBConnection();
            DemoConnection demoConnection = new DemoConnection(mysqlConnection);
            demoConnection.Display();
            Console.ReadLine();
        }
    }
}
