﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqLambdaExpression
{
    public class LetTemporaryValue
    {
        public static void Run()
        {
            int[] number = new int[] { 1, 2, 3, 4, 5, 6 };

            //dont use semicolon at the end of linq expression
            var numberItem = from i in number
            // store result of number's cube inside let, allow to shorten expression (using numberFiltered instead of i*i*i)
                            let numberFiltered = i * i * i
                            where numberFiltered > 100 && numberFiltered < 1000
                            //sorting
                            orderby i descending
                            select i;

            //print list of number which number's cube is < 1000 and > 100
            foreach (var item in numberItem)
            {
                Console.WriteLine(item);
            }
        }
    }
}
