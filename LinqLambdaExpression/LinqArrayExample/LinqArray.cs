﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqLambdaExpression.LinqExample
{
    public class LinqArray
    {
        public static void Run()
        {
            //IEnumerable is parent of list result, so using List<>, Dictionary<> or array can use with IEnumerable
            Student[] students = new Student[2] { new Student(1, "camnh"), new Student(2, "congnv") };

            //linq query returns IEnumerable<>, select smt from collection
            var result = from student in students
                         //using where to filter result, using == comparison
                         where student.Name == "camnh"
                         //a query must end with select or group by
                         //using "from" object directly
                         select student;


            foreach (var item in result)
            {
                Console.WriteLine("Username: " + item.Name);                
                Console.WriteLine("Id: " + item.Id);
            }

            Console.ReadLine();
        }
    }
}
