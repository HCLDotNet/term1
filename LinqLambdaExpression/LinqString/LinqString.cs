﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqLambdaExpression
{
    class LinqString
    {
        public static void Run()
        {
            //Using @ to use string in multi-line, remove @ causes error.
            //String is array of chars
            string str = @"ĐT Anh sẽ có lợi thế được chơi trên sân nhà Wembley trong trận chung kết EURO 2020 với Italia 
                        nhưng thầy trò Gareth Southgate không thể chủ quan bởi đội quân thiên thanh từng 
                        không ít lần gieo sầu cho họ ở các đấu trường lớn. 
                        Cùng xem thầy mo mò tỷ số trận Italia vs Anh diễn ra vào 02h00 ngày 12/7.";

            var spaceFiltered = from i in str
                                   where i == ' '
                                   select i;
            int count = 0;

            foreach (char c in spaceFiltered)
            {
                count++;
            }

            Console.WriteLine("Count is {0}", count);
        }
    }
}
