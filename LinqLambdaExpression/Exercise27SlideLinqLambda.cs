﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqLambdaExpression.LinqExample
{
    class Order
    {
        public int OrderId { get; set; }
        public string ItemName { get; set; }
        public DateTime OrderDate { get; set; }
        public int Quantity { get; set; }

        public Order(int orderId, string itemName, DateTime orderDate, int quantity)
        {
            this.OrderId = orderId;
            this.ItemName = itemName;
            this.Quantity = quantity;
            this.OrderDate = orderDate;
        }
    }

    class Item
    {
        public string ItemName { get; set; }
        public double Price { get; set; }

        public Item(string name, double price)
        {
            this.ItemName = name;
            this.Price = price;
        }
    }

    class OrderItem
    {
        public int OrderId { get; set; }
        public string ItemName { get; set; }
        public double TotalPrice { get; set; }
        public DateTime OrderDate { get; set; }

        public OrderItem (int orderId, string itemName, double totalPrice, DateTime orderDate)
        {
            this.OrderId = orderId;
            this.ItemName = itemName;
            this.TotalPrice = totalPrice;
            this.OrderDate = orderDate;
        }
    }

    class Exercise27SlideLinqLambda
    {
        public static void Run()
        {
            List<Item> items = new List<Item>()
            {
                new Item("computer", 12),
                new Item("desktop", 13),
                new Item("pcs", 14)
            };

            List<Order> orders = new List<Order>()
            {
                new Order(1, "computer", new DateTime(2021, 2,2), 1),
                new Order(2, "desktop", new DateTime(2021,3, 4), 2),
                new Order(2, "pcs", new DateTime(2021,2, 2), 3)
            };

            var result = from i in orders
                         join r in items on i.ItemName equals r.ItemName
                         select new OrderItem(i.OrderId,i.ItemName,i.Quantity * r.Price, i.OrderDate) into order_items              
                         group order_items by order_items.OrderDate.Month into g
                         orderby g.Key descending
                         select g;


            foreach (var monthDetail in result)
            {
                Console.WriteLine("Month: " + monthDetail.Key);
                foreach (var item in monthDetail)
                {
                    Console.WriteLine("  Id: {0} - Name: {1} - Date: {2} - TotalPrice: {3}", item.OrderId,item.ItemName,item.OrderDate.ToLongDateString(), item.TotalPrice);
                }
            }
        }
    }
}
