﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqLambdaExpression
{
    class Order
    {
        public int OrderId { get; set; }
        public string ItemName { get; set; }
        public DateTime OrderDate { get; set; }
        public int Quantity { get; set; }

        public Order (int orderId, string itemName, DateTime orderDate, int quantity)
        {
            this.OrderId = orderId;
            this.ItemName = itemName;
            this.Quantity = quantity;
            this.OrderDate = orderDate;
        }
    }

    class Item
    {
        public string ItemName { get; set; }
        public double Price { get; set; }

        public Item(string name, double price)
        {
            this.ItemName = name;
            this.Price = price;
        }
    }

    class Exercise19SlideLinqLambda
    {
       public static void Run()
       {
            List<Order> orders = new List<Order>()
            {
                new Order(1, "computer", new DateTime(2021, 2,2), 1),
                new Order(2, "desktop", new DateTime(2021,3, 4), 2),
                new Order(2, "pcs", new DateTime(2021,2, 2), 3)
            };

            var result = from i in orders
                             //uncomment for exercise 19
                             //orderby i.OrderDate descending, i.Quantity descending

                             //uncomment to enable slide 21
                            group i by i.OrderDate.Month into g
                            orderby g.Key descending
                            select g;
                   


            //Uncomment for exercise 19
            /**
             * foreach (var item in result)
                {
                    Console.WriteLine("Name: {0} - OrderDate: {1} - Qtity: {2}", item.ItemName, item.OrderDate.ToLongDateString(),item.Quantity);
                }
             */

            //Uncomment for exercise 21
            
             foreach (var monthDetail in result)
             {
                Console.WriteLine("Month: " + monthDetail.Key);
                foreach (var item in monthDetail)
                {
                    Console.WriteLine(" " + item.ItemName);
                }
             }


             
        }
    }
}
