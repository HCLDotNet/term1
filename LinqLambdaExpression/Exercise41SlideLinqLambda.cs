﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqLambdaExpression.Exercise41
{
    class Order
    {
        public int OrderId { get; set; }
        public string ItemName { get; set; }
        public DateTime OrderDate { get; set; }
        public int Quantity { get; set; }

        public Order(int orderId, string itemName, DateTime orderDate, int quantity)
        {
            this.OrderId = orderId;
            this.ItemName = itemName;
            this.Quantity = quantity;
            this.OrderDate = orderDate;
        }
    }

    class Item
    {
        public string ItemName { get; set; }
        public double Price { get; set; }

        public Item(string name, double price)
        {
            this.ItemName = name;
            this.Price = price;
        }
    }

    class OrderItem
    {
        public int OrderId { get; set; }
        public string ItemName { get; set; }
        public double TotalPrice { get; set; }
        public DateTime OrderDate { get; set; }

        public OrderItem(int orderId, string itemName, double totalPrice, DateTime orderDate)
        {
            this.OrderId = orderId;
            this.ItemName = itemName;
            this.TotalPrice = totalPrice;
            this.OrderDate = orderDate;
        }
    }

    class Exercise41SlideLinqLambda
    {
        public static void Run()
        {
            List<Item> items = new List<Item>()
            {
                new Item("computer", 12),
                new Item("desktop", 13),
                new Item("pcs", 14)
            };

            List<Order> orders = new List<Order>()
            {
                new Order(1, "computer", new DateTime(2021, 2,2), 1),
                new Order(2, "desktop", new DateTime(2021,3, 4), 2),
                new Order(3, "pcs", new DateTime(2020,2, 2), 3),
                new Order(4, "pcs", new DateTime(2020,2, 2), 3)

            };

            int[] arr = new int[] { 1,2,3,4,5 };
            var evenNumbers = arr.Where(p => p % 2 == 0);
            Console.WriteLine("------Count of even numbers------");
            Console.WriteLine(arr.Count(p => p%2 == 0));
            Console.WriteLine("------Even numbers------");
            foreach (var item in evenNumbers)
            {
                Console.WriteLine(item);
            }

            Console.WriteLine("------Sum of quantities------");
            var eachItemQuantity = from order in orders
                                   group order by order.ItemName into order_items
                                   select new
                                   {
                                       Key = order_items.Key,
                                       Sum = order_items.Sum(p => p.Quantity),
                                       TotalOrder = order_items.Count()
                                   };
           foreach (var orderItem in eachItemQuantity)
            {
                Console.WriteLine("Name: {0} - Quantity: {1} - Count Order: {2}",orderItem.Key,orderItem.Sum,orderItem.TotalOrder);
            }

            Console.WriteLine("------Item with max order ------");

            var maxOrderItem = eachItemQuantity.OrderByDescending(p => p.TotalOrder).Take(1);
            foreach (var orderItem in maxOrderItem)
            {
                Console.WriteLine("Name: {0} - Quantity: {1} - Count Order: {2}", orderItem.Key, orderItem.Sum, orderItem.TotalOrder);
            }
        }
    }
}
