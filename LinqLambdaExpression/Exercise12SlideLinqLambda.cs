﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqLambdaExpression
{
    class Exercise12SlideLinqLambda
    {
        public static void Run()
        {
            int[] number = new int[] { 1, 2, 3, 4, 5, 6 };

            var numberFiltered = from i in number
                                 where i * i * i < 1000 && i * i * i > 100
                                 select i;

            //print list of number which number's cube is < 1000 and > 100
            foreach (var item in numberFiltered)
            {
                Console.WriteLine(item);
            }
        }
    }
}
