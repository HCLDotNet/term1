﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqLambdaExpression.LinqExample
{
    public class LinqRun
    {
        public static void Run()
        {
            //IEnumerable is parent of list result, so using List<>, Dictionary<> or array can use with IEnumerable
            List<Student> ls = new List<Student>();
            ls.Add(new Student(1, "camnh"));
            ls.Add(new Student(2, "congnv"));

            //linq query returns IEnumerable<>, select smt from collection
            var result = from i in ls
                         //using where to filter result, using == comparison
                         where i.Name == "camnh"
                         //a query must end with select or group by
                         //using alias: Username to mask name data.
                         select new
                         {
                             Username = i.Name,
                             Id = i.Id,
                             UsernameWithId = i.Name + "-" + i.Id
                         };


            foreach (var item in result)
            {
                Console.WriteLine("Username: " + item.Username);
                Console.WriteLine("User with id: " + item.UsernameWithId);
                Console.WriteLine("Id: " + item.Id);
            }

            Console.ReadLine();
        }
    }
}
