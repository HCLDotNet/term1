﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqLambdaExpression.Exercise36
{
    class Order
    {
        public int OrderId { get; set; }
        public string ItemName { get; set; }
        public DateTime OrderDate { get; set; }
        public int Quantity { get; set; }

        public Order(int orderId, string itemName, DateTime orderDate, int quantity)
        {
            this.OrderId = orderId;
            this.ItemName = itemName;
            this.Quantity = quantity;
            this.OrderDate = orderDate;
        }
    }

    class Item
    {
        public string ItemName { get; set; }
        public double Price { get; set; }

        public Item(string name, double price)
        {
            this.ItemName = name;
            this.Price = price;
        }
    }

    class OrderItem
    {
        public int OrderId { get; set; }
        public string ItemName { get; set; }
        public double TotalPrice { get; set; }
        public DateTime OrderDate { get; set; }

        public OrderItem(int orderId, string itemName, double totalPrice, DateTime orderDate)
        {
            this.OrderId = orderId;
            this.ItemName = itemName;
            this.TotalPrice = totalPrice;
            this.OrderDate = orderDate;
        }
    }

    class Exercise36SlideLinqLambda
    {
        public static void Run()
        {
            List<Item> items = new List<Item>()
            {
                new Item("computer", 12),
                new Item("desktop", 13),
                new Item("pcs", 14)
            };

            List<Order> orders = new List<Order>()
            {
                new Order(1, "computer", new DateTime(2021, 2,2), 1),
                new Order(2, "desktop", new DateTime(2021,3, 4), 2),
                new Order(2, "pcs", new DateTime(2020,2, 2), 3)
            };

            var result = orders.Where(p => p.Quantity > 0);

            foreach (var item in result)
            {
                Console.WriteLine("{0} - {1}", item.ItemName, item.Quantity);
            }

            Console.WriteLine("--------Item name ordered in largest quantity ----------");

            var sortedOrders = from order in orders
                                          orderby order.Quantity descending
                                          select order;
            var itemWithLargestQuantity = sortedOrders.Take(1);  
            foreach (var _item in itemWithLargestQuantity)
            {
                Console.WriteLine("Name: {0} - {1}", _item.ItemName, _item.Quantity);
            }

            Console.WriteLine("--------Items placed before Jan this year ----------");
            var placedBeforeJanOrders = orders.Where(p => p.OrderDate < new DateTime(DateTime.Now.Year, 1, 1));
            foreach (var placedBeforeJanOrder in placedBeforeJanOrders)
            {
                Console.WriteLine("Name: {0} - {1}", placedBeforeJanOrder.ItemName, placedBeforeJanOrder.Quantity);
            }
        }
    }
}
