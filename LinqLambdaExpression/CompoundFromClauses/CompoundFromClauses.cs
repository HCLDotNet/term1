﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqLambdaExpression
{
    class CompoundClass
    {
        //using public to access from outside, default is private
        public string first { get; set; }
        public string second { get; set; }

        public CompoundClass (string first,string second)
        {
            this.first = first;
            this.second = second;
        }
    }

    class CompoundFromClauses
    {
        public static void Run()
        {
            string[] fromFirst = new string[] { "dahlia", "rose", "lotus" };
            string[] fromSecond = new string[] { "mango", "apple", "orange", "banana" };

            //Using multiple from require external class
            var fQuery = from from1 in fromFirst
                         from from2 in fromSecond
                         select new CompoundClass(from1, from2);

            foreach (var a in fQuery)
            {
                Console.WriteLine("First: {0}, second: {1}",a.first, a.second);
            }
        }
    }
}
