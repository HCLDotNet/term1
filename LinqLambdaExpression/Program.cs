﻿using LinqLambdaExpression.LinqExample;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqLambdaExpression
{
    class Program
    {
        static void Main(string[] args)
        {
            //Uncomment to use Linq with List
            //LinqRun.Run();

            //uncomment to use Linq with Array
            //LinqArray.Run();

            //uncomment to use Linq with String
            //LinqString.Run();

            //uncomment to use exercise and let to retain linq value
            //Exercise12SlideLinqLambda.Run();

            //LetTemporaryValue.Run();

            //CompoundFromClauses.Run();

            //Exercise16SlideLinqLambda.Run();

            //Exercise19SlideLinqLambda.Run();

            //Exercise27SlideLinqLambda.Run();

            //Exercise29.Exercise29SlideLinqLambda.Run();

            //Exercise36.Exercise36SlideLinqLambda.Run();

            Exercise41.Exercise41SlideLinqLambda.Run();

            Console.ReadLine();
        }
    }
}
