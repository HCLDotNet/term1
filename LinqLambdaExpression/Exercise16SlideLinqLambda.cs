﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqLambdaExpression
{
    class ParticipantMatch
    {
        public Participant participant1;
        public Participant participant2;

        public ParticipantMatch(Participant participant1, Participant participant2)
        {
            this.participant1 = participant1;
            this.participant2 = participant2;
        }
    }

    class Participant
    {
        public string Name { get; set; }
        public string Country { get; set; }

        public Participant(string name,string country)
        {
            this.Name = name;
            this.Country = country;
        }
    }

    class Exercise16SlideLinqLambda
    {
        public static void Run()
        {
            Participant[] list = new Participant[]
            {
                new Participant("camnh", "vietnam"),
                new Participant("congnv", "usa"),
                new Participant("ngoctb", "vietnam"),
                new Participant("congnv", "congo"),
                new Participant("tranglt", "usa")
            };

            Participant[] participants1 = new Participant[list.Length / 2];

            for (int i = 0; i < list.Length / 2; i++)
            {
                participants1[i] = list[i];
            }

            int remaining = list.Length % 2 == 0 ? list.Length / 2 : list.Length / 2 + 1;

            Participant[] participants2 = new Participant[remaining];

            for (int i = 0; i < remaining; i++)
            {
                participants2[i] = list[list.Length / 2 + i];
            }

            var query = from participant1 in participants1
                        from participant2 in participants2
                        where participant1.Country != participant2.Country
                        select new ParticipantMatch(participant1, participant2);
            foreach (var item in query)
            {
                Console.WriteLine("Participant 1[name:{0}, country:{1}] - Participant 2[name:{2}, country:{3}]",
                    item.participant1.Name, item.participant1.Country, item.participant2.Name, item.participant2.Country);
            }
        }
    }
}
