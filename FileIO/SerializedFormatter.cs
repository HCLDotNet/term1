﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace FileIO
{
    class SerializedFormatter
    {
        public static void Run()
        {
            SerializedUser user = new SerializedUser("abc", 12);
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            using (FileStream fileStream = new FileStream(@"D:\serialized.txt", FileMode.OpenOrCreate))
            {
                binaryFormatter.Serialize(fileStream, user);
            }
        }
    }

    class DeserializeFormatter
    {
        public static void Run()
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream fileStream = new FileStream(@"D:\serialized.txt", FileMode.Open);
            SerializedUser s = (SerializedUser)binaryFormatter.Deserialize(fileStream);
            Console.WriteLine("Name: {0}", s.Name);
            Console.WriteLine("Age: {0}", s.Age);
        }
    }
}
