﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileIO
{
    class Exercise16SlideIO
    {
        public static void Create()
        {
            Console.WriteLine("------------Exercise Slide16 IO -----------");
            string path = @"D:\Temps";
            //if directory is not exist, then create the path
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
            //Create file in docx, xlsx, txt
            //Remember to close FileStream 
            FileStream xlsx = File.Create(path + @"\abc.xlsx");
            xlsx.Close();
            FileStream docx = File.Create(path + @"\abc.docx");
            docx.Close();
            FileStream txt = File.Create(path + @"\abc.txt");
            txt.Close();

            Directory.CreateDirectory(path + @"\TextFiles");

            //Move txt files to TextFiles folder

            DirectoryInfo directoryInfo = new DirectoryInfo(path);
            FileInfo[] fileInfo = directoryInfo.GetFiles("*.txt");            
            foreach (FileInfo file in fileInfo)
            {
                if (file.Exists)
                {
                    //rename from .txt to dat
                    string replacedFileName = file.Name.Replace(".txt", ".dat");
                    file.MoveTo(path + @"\TextFiles\" + replacedFileName);                      
                }
            }
            Console.ReadLine();
        }
    }
}
