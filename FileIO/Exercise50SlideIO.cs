﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FileIO
{
    [Serializable]
    public class Salary
    {
        int basicSalary;
        int HRA;
        int TA;
        
        public int HRANumber { get { return this.HRA; } set { this.HRA = value; } }
        public int BasicSalary { get { return this.basicSalary; } set { this.basicSalary = value; } }
        public int TANumber { get { return this.TA; } set { this.TA = value; } }

        public Salary(int basicSalary, int HRA, int TA)
        {
            this.basicSalary = basicSalary;
            this.HRA = HRA;
            this.TA = TA;
        }
        public Salary()
        {

        }
    }

    [Serializable]
    public class Employee
    {
        public long employeeId { get; set; }

        [XmlAttribute(AttributeName = "type")]
        public string type { get; set; }

        public string name { get; set; }

        public Salary salary { get; set; }

        public Employee()
        {

        }
        public Employee(long employeeId, string type, string name, Salary salary)
        {
            this.employeeId = employeeId;
            this.type = type;
            this.name = name;
            this.SetSalary(salary);
        }

        public void SetSalary(Salary s)
        {
            if (this.type == "Contract")
            {
                s.HRANumber = 0;
                this.salary = s;
            }
            else
            {
                this.salary = s;
            }         
        }

        public void PrintInfo()
        {
            Console.WriteLine("Employee name: {0}", name);
            Console.WriteLine("Employee type: {0}", type);
            Console.WriteLine("Salary details: Basic:{0}, HRA:{1}, TA: {2}", salary.BasicSalary, salary.HRANumber, salary.TANumber);
        }
    }

    class Exercise50SlideIO
    {
        public static void Run()
        {
            //Simulate employee input from user by hard code object of class            
            Salary s = new Salary(120, 12, 4);
            Salary s1 = new Salary(120, 12, 4);
            Employee e = new Employee(1, "Contract", "camnh", s);
            Employee e1 = new Employee(2, "FullTime", "congnv", s1);

            Employee[] employees = new Employee[2];
            employees[0] = e;
            employees[1] = e1;

            using (TextWriter textWriter = new StreamWriter(@"C:\Users\camnh\Desktop\stream.xml"))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(Employee[]));
                //in order to serialize, class must have setter and getter
                xmlSerializer.Serialize(textWriter, employees);
            }             
        }
    }
}
