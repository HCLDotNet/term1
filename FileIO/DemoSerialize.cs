﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace FileIO
{
    [Serializable]
    class User
    {
        int id;
        [NonSerialized]
        string name;

        public User(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public int Id { get { return this.id;  } set { this.id = value; } }
        public string Name { get { return this.name; } set { this.name = value; } }
    }

    public class Customer
    {

        public Customer()
        {

        }
        public Customer(int id, string name, int age)
        {
            Id = id;
            Name = name;
            Age = age;
        }

        [XmlAttribute(AttributeName ="MyId")]
        public int Id { get; set; }
        [XmlAttribute(AttributeName ="MyName")]
        public string Name { get; set; }
        public int Age { get; set; }
    }

    class DemoSerialize
    {
        public static void Run()
        {
            string path = @"C:\Users\camnh\Desktop\userserialized.txt";
            User u = new User(1, "camnh");
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            FileStream fileStream = new FileStream(path, FileMode.OpenOrCreate);
            binaryFormatter.Serialize(fileStream, u);
            fileStream.Close();
            //
            Console.WriteLine("=================Deserialized ===============");
            FileStream fileStream1 = new FileStream(path, FileMode.OpenOrCreate);
            User c = (User)binaryFormatter.Deserialize(fileStream1);
            Console.WriteLine(c.Id + "===" +c.Name);
            fileStream1.Close();

            Console.WriteLine("=================Xml serialize ===============");
            string path2 = @"C:\Users\camnh\Desktop\userserialized.xml";
            FileStream fileStream2 = new FileStream(path2, FileMode.OpenOrCreate);
            XmlSerializer xMLSerializer = new XmlSerializer(typeof(List<Customer>));
            List<Customer> ls = new List<Customer>();
            ls.Add(new Customer(1, "camnh", 2));
            ls.Add(new Customer(2, "ngoctb", 3));
            xMLSerializer.Serialize(fileStream2, ls);
            fileStream2.Close();

            Console.WriteLine("=================Xml deserialize ===============");
            FileStream fileStream3 = new FileStream(path2, FileMode.OpenOrCreate);
            XmlSerializer xMLSerializer1 = new XmlSerializer(typeof(Customer));
            
            List<Customer> cuD = (List<Customer>)xMLSerializer.Deserialize(fileStream3);
            foreach (Customer cD in cuD)
            {
                Console.WriteLine(cD.Id + "" + cD.Name + ""+cD.Age);
            }

            
        }
    }
}
