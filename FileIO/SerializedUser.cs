﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileIO
{
    [Serializable]
    public class SerializedUser
    {
        // a field, mark as non serialized, is not a part of serialization
        [NonSerialized]
        string _name;

        int _age;

        public string Name {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }
        
        public int Age {
            get {
                return _age;              
            }
            set
            {
                _age = value;
            }
        }

        public SerializedUser(string name, int age)
        {
            Name = name;
            Age = age;
        }
    }
}
