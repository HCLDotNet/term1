﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Xml.Serialization;

namespace FileIO.Inclass
{
    //omit this ==> serializable cannot continue
    [Serializable]
    public class Customer
    {
        int id;
        string name;
        [NonSerialized]
        string password;

        public Customer(int id, string name, string password)
        {
            this.id = id;
            this.name = name;
            this.password = password;
        }

        public int Id {
            get { return this.id; }
        }
        public string Name {
           get { return this.name; }
        }

        public string Password
        {
            get
            {
                return this.password;
            }
        }
    }

    
    public class Employee
    {
        [XmlAttribute(AttributeName = "myId")]
        public int Id { get; set; }

        public string Name { get; set;}

        public Employee() { }

        public Employee(int id, string name)
        {
            Id = id;
            Name = name;    
        }
    }

    class SerializationDataInClass
    {
        public static void Run()
        {
            string path = @"C:\Users\camnh\Desktop\customerinclass.txt";
            FileStream fileStream = new FileStream(path, FileMode.OpenOrCreate);
            List<Customer> ls = new List<Customer>();
            Customer c = new Customer(1, "camnh", "123456"); //serialize this
            Customer c1 = new Customer(2, "camnh1", "1234567");
            Customer c2 = new Customer(3, "camnh2", "123456789");
            ls.Add(c);
            ls.Add(c1);
            ls.Add(c2);

            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(fileStream, ls);
            fileStream.Close();

            Console.WriteLine("=====================Deserialization==============");
            FileStream fileStream1 = new FileStream(path, FileMode.OpenOrCreate);
            List<Customer> returnCus = (List<Customer>)binaryFormatter.Deserialize(fileStream1);

            foreach (Customer _c in returnCus)
            {
                Console.WriteLine("ID: {0}, Name: {1} Password: {2}", _c.Id, _c.Name, _c.Password);
            }
            fileStream1.Close();

            Console.WriteLine("=====================Serialize data in xml==============");
            string pathXml = @"C:\Users\camnh\Desktop\customerinclass.xml";
            FileStream fileStreamXml = new FileStream(pathXml, FileMode.OpenOrCreate);
            Employee e = new Employee(1, "camnh");
            XmlSerializer xmlSerializer = new XmlSerializer(typeof(Employee));
            xmlSerializer.Serialize(fileStreamXml, e);
            fileStreamXml.Close();

            Console.WriteLine("=====================DeSerialize data in xml==============");
            FileStream fileStreamXmlDeserialize = new FileStream(pathXml, FileMode.OpenOrCreate);
            Employee deserializedEmployee = (Employee)xmlSerializer.Deserialize(fileStreamXmlDeserialize);
            Console.WriteLine("ID: {0} Name: {1}", deserializedEmployee.Id, deserializedEmployee.Name);

            Console.WriteLine("=====================Serialize list in xml==============");
            string pathXml1 = @"C:\Users\camnh\Desktop\customerinclasslist.xml";
            FileStream fileStreamXmlList = new FileStream(pathXml1, FileMode.OpenOrCreate);
            List<Employee> empList = new List<Employee>();
            Employee e1 = new Employee(1, "camnh");
            Employee e2 = new Employee(2, "camnh1");
            empList.Add(e1);
            empList.Add(e2);
            XmlSerializer xmlSerializer1 = new XmlSerializer(typeof(List<Employee>));
            xmlSerializer1.Serialize(fileStreamXmlList, empList);
            fileStreamXmlList.Close();

            Console.WriteLine("=====================DeSerialize list in xml==============");
            FileStream fileStreamXmlListDeserialize = new FileStream(pathXml1, FileMode.OpenOrCreate);
            //??
            List<Employee> employeeDeserializedList = (List<Employee>)xmlSerializer1.Deserialize(fileStreamXmlListDeserialize);
            foreach (Employee deserializedEmployeeItem in employeeDeserializedList)
            {
                Console.WriteLine("ID: {0}, Name: {1}", deserializedEmployeeItem.Id, deserializedEmployeeItem.Name);
            }
        }
    }
}
