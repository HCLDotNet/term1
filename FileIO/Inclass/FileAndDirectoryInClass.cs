﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace FileIO.Inclass
{
    class FileAndDirectoryInClass
    {
        public static void Run()
        {
            try
            {
                //
                //priviledge to create new.
                string path = @"C:\Users\camnh\Desktop\MyInClass\my.txt";
                

                Console.WriteLine("================StreamReader: Read all line==================");
                using (StreamReader reader1 = new StreamReader(path))
                {
                    Console.WriteLine(reader1.ReadToEnd().Substring(190,260));
                }

                //create logfile: log.txt
                //try to produce error
                //write error into log.txt
                //2things
                //error name
                //error stack trace

                
                //hello
                //hi
                //abc
                //def
                //done
                //=> 4words
                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
            

            //Directory.CreateDirectory(path);
        }
    }
}
