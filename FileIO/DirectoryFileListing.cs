﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileIO
{
    class DirectoryFileListing
    {
        public static void Run()
        {
            //Specify directory to use
            string path = @"D:\mama";
            DirectoryInfo dI = new DirectoryInfo(path);

            //if directory is not exist, then create the path
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);

            }

            //list files under directory;
            FileInfo[] files = dI.GetFiles();
            for (int i = 0; i < files.Length; i++)
            {
                Console.WriteLine(files[i].Name);
            }

            //Create some files, similar to Slide16
            string excelPath = path + @"\excel1.xlsx";
            File.Create(excelPath);
        }
    }
}
