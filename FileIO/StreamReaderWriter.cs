﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileIO
{
    class StreamReaderWriter
    {
        public static void Run()
        {
            Console.WriteLine("Enter infor");
            string input = Console.ReadLine();
            string path = @"D:\exercise21.txt";

            FileStream fileStream = new FileStream(path, FileMode.Append, FileAccess.Write);
            StreamWriter streamWriter = new StreamWriter(fileStream);
            
                //write a line to file
                streamWriter.WriteLine(input);

            //if do not close then errors occurs: StreamWriter is currently using at line 31->37.
            //streamWriter.Close();//streamWriter.Dispose()

            /**
             * Reading from file, using another FileStream
             */
            FileStream anotherFileStream = new FileStream(path, FileMode.OpenOrCreate);
            using (StreamReader streamReader = new StreamReader(anotherFileStream))
            {
                string output = streamReader.ReadLine();
                Console.WriteLine(output);
                string[] printToScreen = output.Split(' ');
                Console.WriteLine("Number of words: " + printToScreen.Length);
                Console.ReadLine();
            }
            anotherFileStream.Close();
        }
    }
}
