﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileIO
{
    class Exercise21SlideIO
    {
        public static void Run()
        {
            string userInput = "";
            char[] stops = new char[] { 'd', 'o', 'n', 'e' };
            string path = @"C:\Users\camnh\Desktop\MyInClass\tmp.txt";
            Console.WriteLine("Enter input");
            int i = 0;
            while (i <= 3)
            {
                //using readkey to capture key input
                ConsoleKeyInfo keyInput = Console.ReadKey();               
                userInput += keyInput.KeyChar;
                if (keyInput.KeyChar == stops[i])
                {                    
                    i++;
                }
                else
                {
                    i = 0;
                }
                
            }

            Console.WriteLine();

            //each element of string[] is positioned in different lines
            File.WriteAllLines(path, userInput.Split(' '));

            //read all lines and print out number of words inside file
            Console.WriteLine(File.ReadAllLines(path).Length);

        }
    }
}
