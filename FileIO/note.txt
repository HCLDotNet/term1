﻿1. source to sink
writing: write from program to files : source: program / sink: file
reading: read from file to program: source: file content from file / program: display content of file to console.

byte-oriented: 
	1. Helloworld => 0F 2E 4C 34 ...
	2. Write array of bytes into file (Write)
		FileStream.
write: 1 stream
read: 1 stream

character oriented
	1. Text: Helloworld
	2. Write Helloworld directly into file without transforming to bytes.
		StreamWriter / StreamReader

Stream is parent of all stream: FileStream / StreamReader / StreamWriter based on Stream.