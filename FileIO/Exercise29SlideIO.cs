﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileIO
{
    class Exercise29SlideIO
    {
        public static void Run()
        {
            string path = @"D:\colon";            
            DirectoryInfo dI = new DirectoryInfo(path);
            FileInfo[] files = dI.GetFiles("*.cs");
            
            foreach (FileInfo file in files)
            {
                byte[] replacedToSemicolon = new byte[] { };
                byte[] arr = new byte[] { };
                int numBytesToRead = 0;
                using (FileStream fileStream = file.OpenRead())
                {
                    arr = new byte[fileStream.Length];
                    UTF8Encoding data = new UTF8Encoding();
                    numBytesToRead = (int)fileStream.Length;
                    int c;
                    while ((c = fileStream.Read(arr, 0, numBytesToRead)) > 0)
                    {
                        replacedToSemicolon = Encoding.UTF8.GetBytes(Encoding.UTF8.GetString(arr, 0, numBytesToRead).Replace(':', ';'));
                    }
                }

                using (FileStream fileStreamWrite = file.OpenWrite())
                {
                    fileStreamWrite.Write(replacedToSemicolon, 0, replacedToSemicolon.Length);
                }
            }
        }
    }
}
