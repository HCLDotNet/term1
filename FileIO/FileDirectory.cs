﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileIO
{
    class FileDirectory
    {
        public static void Run()
        {
            try
            {
                string path = @"C:\Users\camnh\Desktop\My";
                //DirectoryInfo d = Directory.CreateDirectory(path);

                DirectoryInfo d = new DirectoryInfo(path);
                FileInfo[] files = d.GetFiles();
                foreach (FileInfo file in files)
                {
                    Console.WriteLine(file.FullName);
                }

                File.Create(path + "\\ttt.txt");

                //Write something
                FileStream fileStream = new FileStream(path + "\\my.txt", FileMode.OpenOrCreate);
                string sentence = @"Hello how are you";
                UTF8Encoding uTF8Encoding = new UTF8Encoding();
                byte[] b = uTF8Encoding.GetBytes(sentence);
                fileStream.Write(b, 0, b.Length);
                fileStream.Close();
                
                //read
                Console.WriteLine("--------------Reading---------------");
                FileStream fileStream1 = new FileStream(path + "\\my.txt", FileMode.OpenOrCreate);
                byte[] read = new byte[fileStream1.Length];
                string s = "";
                while (fileStream1.Read(read, 0, Convert.ToInt32(fileStream1.Length)) > 0)
                {
                    s += uTF8Encoding.GetString(read, 0, Convert.ToInt32(fileStream1.Length));
                }

                Console.WriteLine(s);
                fileStream1.Close();

                //string
                FileStream fileStream2 = new FileStream(path + "\\sample.txt", FileMode.OpenOrCreate);
                StreamWriter writer = new StreamWriter(fileStream2, Encoding.UTF8);
                writer.WriteLine("oh my god");
                writer.Close();
                fileStream2.Close();

                //reader
                Console.WriteLine("--------------Using stream reader---------------");
                FileStream fileStream3 = new FileStream(path + "\\sample.txt", FileMode.OpenOrCreate);
                StreamReader reader = new StreamReader(fileStream3);
                string mys = null;
                while ((mys = reader.ReadLine()) != null)
                {
                    Console.WriteLine(mys);
                }
                
                reader.Close();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
