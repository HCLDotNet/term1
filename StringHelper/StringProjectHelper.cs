﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StringHelper
{
    public class StringProjectHelper
    {
        public string ToLower(string s)
        {
            return s.ToLower();
        }

        public string ToUpper(string s)
        {
            return s.ToUpper();
        }
    }
}
