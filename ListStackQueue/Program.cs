﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ListStackQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            //initiate a List
            List<string> ls = new List<string>();
            ls.Add("1");
            ls.Add("2");
            ls.Add("3");

            //Print items in the list
            PrintList(ls);

            //Get item at specified index
            Console.WriteLine("Value at index 0 is " + ls[0]);

            //Find index of item
            Console.WriteLine("Index of '1' is " + ls.IndexOf("1"));

            //Length of list
            Console.WriteLine("Length of the list is " + ls.Count);
            /**
             * Remove element in list using value
             */
            Console.WriteLine("After removing item in a list");
            ls.Remove("2"); //return 1 3
            PrintList(ls);

            /**
             * Remove element in list using index
             */
            Console.WriteLine("After removing item in a list using index");
            //speicifing index to remove item at index 0
            ls.RemoveAt(0);
            PrintList(ls);

            /**
             * Using queue
             */
            Console.WriteLine("----------Queue--------------");
            usingQueue();

            /**
             * Using stack
             */
            Console.WriteLine("----------Stack--------------");
            usingStack();
            Console.ReadLine();

        }

        static void usingQueue()
        {
            //Represent FIFO principle
            Queue<string> q = new Queue<string>();
            //add element to queue
            q.Enqueue("a");
            q.Enqueue("b");
            q.Enqueue("c");
            PrintQueue(q);
            //remove element from queue
            Console.WriteLine("Remove element from queue");
            q.Dequeue(); //print "b" "c"
            PrintQueue(q);
            

            //find last element at queue
            Console.WriteLine("Last element of queue: " + q.Last());
            //find first element at queue
            Console.WriteLine("First element of queue: " + q.First());
        }

        static void usingStack()
        {
            //Represent LIFO
            Stack<string> stack = new Stack<string>();

            //Push item to stack
            stack.Push("stack 1");
            stack.Push("stack 2");
            stack.Push("stack 3");
            PrintStack(stack);

            //Pop
            Console.WriteLine("Pop item out of stack");
            stack.Pop();
            //Print stack
            PrintStack(stack);
        }

        static void PrintStack(Stack<string> s)
        {
            foreach (string item in s)
            {
                Console.WriteLine(item);
            }
        }

        static void PrintQueue(Queue<string> q)
        {
            Console.WriteLine("Queue element");
            foreach (string item in q)
            {
                Console.WriteLine(item);
            }
        }

        static void PrintList(List<string> ls)
        {
            foreach (string item in ls)
            {
                Console.WriteLine(item);
            }
        }
    }
}
