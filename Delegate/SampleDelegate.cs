﻿using System;
namespace Delegate
{
    public class SampleDelegate
    {
        public delegate void Operation(string s);

        public void Add()
        {
            Console.WriteLine("Add");
        }

        public void Substract()
        {
            Console.WriteLine("Substract");
        }

        public static void Run()
        {
            SampleDelegate sampleDelegate = new SampleDelegate();

            //Operation operation = new Operation(sampleDelegate.Add);//1st syntax //OOP
            //Operation op = sampleDelegate.Add //2nd syntax //declaration
            //operation += sampleDelegate.Substract;

            //operation.Invoke();

            Operation op = delegate (string s) {
                Console.WriteLine(s);

	        };

            op.Invoke("s");
        }
    }
}
