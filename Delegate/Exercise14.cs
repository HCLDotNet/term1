﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate
{
    public delegate double Convert(double VND);

    class Exercise14
    {
        public static double ConvertToUSD(double VND)
        {
            return VND / 23000;
        }

        public static double ConvertToJPY(double VND)
        {
            return VND / 39000;
        }

        public static void Run()
        {
            Convert convert = new Convert(ConvertToUSD);
            double toUSD = convert.Invoke(20000);
            Console.WriteLine("ToUSD" +toUSD);

            Convert convert1 = new Convert(ConvertToJPY);
            double toJPY = convert1.Invoke(20000);
            Console.WriteLine("ToJPY" + toJPY);
        }
    }
}
