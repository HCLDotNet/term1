﻿using System;
namespace Delegate.InClass
{
    public  class MySampleSubscriber
    {
        public void SendMail(string s)
        {
            Console.WriteLine("Sending mail {0}",s);
        }

        public void WriteToLog(string s)
        {
            Console.WriteLine("Write to log: {0}",s);
        }
    }
}
