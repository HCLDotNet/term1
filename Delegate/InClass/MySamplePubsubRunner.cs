﻿using System;
namespace Delegate.InClass
{
    public class MySamplePubsubRunner
    {
        public static void Run()
        {
            MySamplePublisher mySamplePublisher = new MySamplePublisher();
            MySampleSubscriber mySampleSubscriber = new MySampleSubscriber();

            //access event
            mySamplePublisher.MySamplePublisherEventHandler += mySampleSubscriber.SendMail;
            mySamplePublisher.MySamplePublisherEventHandler += mySampleSubscriber.WriteToLog;

            mySamplePublisher.PublishMessage();
        }
    }
}
