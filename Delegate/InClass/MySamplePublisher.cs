﻿using System;
namespace Delegate.InClass
{
    public delegate void MyEventHandler(string s);

    public class MySamplePublisher
    {
        //declare an event == Click
        public event MyEventHandler MySamplePublisherEventHandler;

        //main implementationn
        public void PublishMessage()
        {
            //Main implementation
            Console.WriteLine("Hello");

            //follow-up implementations: listener
            OnPublishedMessageSent();
        }

        //event occured.
        public void OnPublishedMessageSent()
        {
            MySamplePublisherEventHandler.Invoke("Published successfully");
        }
    }
}
