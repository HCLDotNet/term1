﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegate
{
    public delegate void PrintSomething(string str);

    class Usefulness
    {
        int id;
        string name;

        public Usefulness(int id, string name)
        {
            this.id = id;
            this.name = name;
        }

        public static void AfterCreate()
        {
            PrintSomething printSomething = new PrintSomething(PrintLetter);
            printSomething += PrintCharacter;

            printSomething.Invoke("abc");
        }

        public static void PrintLetter(string a)
        {
            Console.WriteLine(a);
        }

        public static void PrintCharacter(string a)
        {
            Console.WriteLine("Character" + a);
        }


        public static void Run()
        {
            Usefulness u = new Usefulness(1, "camnh");
            AfterCreate();
        }
    }
}
