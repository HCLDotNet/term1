﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Delegate.EventObjectSender;
using Delegate.InClass;
using Delegate.ProducerConsumer;

namespace Delegate
{
    public class Program
    {
        public static void Main(string[] args)
        {
            SampleDelegate.Run();

            //DelegateWithParamsRunner.Run();

            //CovarianceAndContraVariance.Run();

            //DelegateDemo.Run();

            //ExRunner.Run();

            //EventDelegate.Run();

            //EventHandlerWithParams.Run();

            //ProducerConsumerRunner.Run();

            //MySamplePubsubRunner.Run();

            //EventObjectSenderRunner.Run();

            Console.ReadLine();
        }
    }
}
