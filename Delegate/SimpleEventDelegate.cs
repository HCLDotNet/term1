﻿using System;
namespace Delegate
{
    public delegate void DelEventHandler();

    class SimpleEventDelegate
    {
        public static event DelEventHandler add;

        public static void Run()
        {
            add += new DelEventHandler(USA);
            add += new DelEventHandler(India);
            add += new DelEventHandler(England);
            add.Invoke();

            Console.ReadLine();
        }

        static void USA()
        {
            Console.WriteLine("USA");
        }

        static void India()
        {
            Console.WriteLine("India");
        }

        static void England()
        {
            Console.WriteLine("England");
        }
    }
}
