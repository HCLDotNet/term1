﻿using System;

namespace Delegate
{
    public delegate void EventHandler(string a);

    public class Operation
    {
        public event EventHandler xyz;

        public void Action(string a)
        {
            if (xyz != null)
            {
                xyz(a);
                Console.WriteLine(a);
            }
            else
            {
                Console.WriteLine("Not Registered");
            }
        }
    }

    class EventHandlerWithParams
    {
        public static void CatchEvent(string s)
        {
            Console.WriteLine("Method Calling");
        }

        public static void Run()
        {
            Operation o = new Operation();

            o.xyz += new EventHandler(CatchEvent);
            o.Action("Event Calling");

            Console.ReadLine();
        }
    }
}