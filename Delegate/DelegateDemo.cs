﻿using System;
namespace Delegate
{
    //access-modifier delegate return_type method_name(Parameter if necessary)

    //PrintSomething():Similar to contract, all methods need to follow syntax (return type and params)
    public delegate void PrintSomething();

    public class DelegateDemo
    {
        public static void PrintA()
        {
            Console.WriteLine("A");
        }

        public static void PrintB()
        {
            Console.WriteLine("B");
        }

        public static void Run()
        {
            //single cast delegate.
            PrintSomething printSomething = PrintA;
            //multicast
            printSomething += PrintB;

            printSomething.Invoke();//Invoke
        }
    }

    
}
