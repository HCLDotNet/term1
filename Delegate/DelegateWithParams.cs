﻿using System;
using static Delegate.DelegateWithParams;

namespace Delegate
{
    public delegate int CalculateWithParams(int a, int b);

    public class DelegateWithParams
    {

        public int Sum(int a, int b)
        {
            return a + b;
        }

        public int Substract(int a, int b)
        {
            return a - b;
        }
    }

    public class DelegateWithParamsRunner
    {
        public static void Run()
        {
            #region DelegateWithFunction
            //1st way
            DelegateWithParams delegateWithParams = new DelegateWithParams();

            CalculateWithParams calculateWithParams = new CalculateWithParams(delegateWithParams.Sum);
            Console.WriteLine(calculateWithParams.Invoke(1, 2));

            calculateWithParams += delegateWithParams.Substract;
            Console.WriteLine(calculateWithParams.Invoke(1, 2));
            #endregion

            #region Anonymous method
            CalculateWithParams c = delegate (int a, int b)
            {
                return a + b;
            };

            Console.WriteLine(c.Invoke(1, 2));
            #endregion
        }
    }
}
