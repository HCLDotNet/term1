﻿using System;
namespace Delegate.ProducerConsumer
{
    public class ProducerConsumerRunner
    {
        public static void Run()
        {
            Producer p = new Producer();
            Consumer c = new Consumer();
            p.ProducerEventHandler += new MyEventHandler(c.EventSubscriber);
            p.ProducerEventHandler += new MyEventHandler(c.AnotherEventRaised);
            p.DoingThing();
        }
    }
}
