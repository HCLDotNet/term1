﻿using System;
namespace Delegate.ProducerConsumer
{
    public delegate void MyEventHandler(string s);

    public class Producer
    {
        public event MyEventHandler ProducerEventHandler;

        public void DoingThing()
        {
            Console.WriteLine("Doing things");
            OnEventOccur();
        }

        public void OnEventOccur()
        {
            if (ProducerEventHandler != null)
            {
                ProducerEventHandler("event raised");
            }
        }
    }
}
