﻿using System;
namespace Delegate.ProducerConsumer
{
    public class Consumer
    { 
        public void EventSubscriber(string s)
        {
            Console.WriteLine("Sender: {0}, Args: {1}", "", s);
        }

        public void AnotherEventRaised(string a)
        {
            Console.WriteLine("Oop: {0} {1}", "", a);
        }
    }
}
