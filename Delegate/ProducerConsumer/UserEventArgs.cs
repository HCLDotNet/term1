﻿using System;
namespace Delegate.ProducerConsumer
{
    public class UserEventArgs : EventArgs
    {
        public UserEventArgs(int id, string username)
        {
            this.Id = id;
            this.Username = username;
        }

        public int Id { get; set; }

        public string Username { get; set; }
    }
}
