﻿using System;
namespace Delegate.EventObjectSender
{
    public class EventObjectSenderRunner
    {
        public static void Run()
        {
            EventObjectSenderPublisher eventObjectSenderPublisher = new EventObjectSenderPublisher();
            EventObjectSenderSubscriber eventObjectSenderSubscriber = new EventObjectSenderSubscriber();

            //attach event
            eventObjectSenderPublisher.MyObjectSenderEventHandlerEvent += eventObjectSenderSubscriber.OnFirstAction;
            eventObjectSenderPublisher.MyObjectSenderEventHandlerEvent += eventObjectSenderSubscriber.OnSecondAction;

            //lastly, execute main implementation
            eventObjectSenderPublisher.SendMessage();
        }
    }
}
