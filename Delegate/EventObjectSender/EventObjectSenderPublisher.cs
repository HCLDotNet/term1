﻿using System;
namespace Delegate.EventObjectSender
{
    public delegate void MyObjectSenderEventHandler(object sender, CustomerEventArgs customerEventArgs);

    public class CustomerEventArgs : EventArgs
    {
        public CustomerEventArgs(int id, string name)
        {
            Id = id;
            Name = name;
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class EventObjectSenderPublisher
    {
        //1. Declare an event
        public event MyObjectSenderEventHandler MyObjectSenderEventHandlerEvent;

        //2. Raise a main event
        public void SendMessage()
        {
            //Main implementation
            Console.WriteLine("Main impl");

            //Event
            OnMessageSent();
        }

        public void OnMessageSent()
        {
            MyObjectSenderEventHandlerEvent.Invoke(this, new CustomerEventArgs(1, "camnh"));
        }
    }
}
