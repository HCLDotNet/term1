﻿using System;
namespace Delegate.EventObjectSender
{
    public class EventObjectSenderSubscriber
    {
        public void OnFirstAction(object sender, CustomerEventArgs customerEventArgs)
        {
            Console.WriteLine("Sender: {0}, arguments: Id: {1} --- Name {2}", sender.ToString(), customerEventArgs.Id, customerEventArgs.Name);
        }

        public void OnSecondAction(object sender, CustomerEventArgs customerEventArgs)
        {
            Console.WriteLine("On secondaction Sender: {0}, arguments: Id: {1} --- Name {2}",
                sender.ToString(), customerEventArgs.Id, customerEventArgs.Name);
        }
    }
}
