﻿using System;

namespace Delegate
{
    delegate void Convert(double VND);

    public class Ex
    {
        public void convertToUSD(double VND)
        {
            Console.WriteLine(VND / 23000);
        }

        public void convertToJPY(double VND)
        {
            Console.WriteLine(VND / 39000);
        }

    }

    public class ExRunner
    {
        public static void Run()
        {
            //why fails ??
            Ex myExercise = new Ex();

            Convert c = new Convert(myExercise.convertToUSD);
            //double convertToUSD = c.Invoke(20000);//Invoke return double on delegate
            //Console.WriteLine(convertToUSD);

            //Convert c1 = new Convert(myExercise.convertToJPY);
            //Console.WriteLine(c1.Invoke(20000));
            //multicast delegate: a delegate can apply for more than 1 method
            c += myExercise.convertToJPY;

            c.Invoke(20000);

        }
        
    }
}
