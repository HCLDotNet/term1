﻿using System;
namespace Delegate
{
    //can request smaller type on return type: Small
    public delegate Small CoverDelegate(Big mc);

    //Small -> Big -> Bigger
    //base
    public class Small
    {

    }

    //more derived
    public class Big : Small
    {

    }

    //more derived
    public class Bigger : Big
    {

    }

    public class CovarianceAndContraVariance
    {
        public CovarianceAndContraVariance()
        {

        }

        //Big in return type is smaller than Small on delegate. This is covariance
        public static Big Method1(Big big)
        {
            Console.WriteLine("Method1");

            return new Big();
        }

        //Small in param is greater than Big. This is contrainvariance
        public static Small Method2(Small small)
        {
            Console.WriteLine("Method2");
            return new Small();
        }

        public static void Run()
        {
            CoverDelegate coverDelegate = new CoverDelegate(Method1);
            coverDelegate += Method2;

            Small s = coverDelegate.Invoke(new Bigger());
        }
    }


}
