﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Inheritance
{
    class Person
    {
        //protected and public properties are inherited by child classes
        protected int Id;
        protected string Name;
        protected int Age;

        public Person(int id, string name, int age)
        {
            Id = id;
            Name = name;
            Age = age;
        }

        public virtual void DisplayInformation()
        {
            Console.WriteLine("Employee id: " + Id);
            Console.WriteLine("Employee name: " + Name);
            Console.WriteLine("Employee age: " + Age);
        }

        public void DisplaySameInformation()
        {
            Console.WriteLine("Employee id: " + Id);
            Console.WriteLine("Employee name: " + Name);
            Console.WriteLine("Employee age: " + Age);
        }
    }

    //inheritance in C# using semicolon symbol
    class Employee : Person
    {
        // if access modifier is not stated, scope is private
        string WorkingAddress;

        // using base() to specify parent constructor, similar to "parent" in java
        public Employee(int id, string name, int age, string workingAddress) : base(id, name, age)
        {
            WorkingAddress = workingAddress;
        }

        //using base. to call methods from parent
        //using override allows child class can override method from parent class.
        public override void DisplayInformation()
        {
            base.DisplayInformation();
            Console.WriteLine("Employee working address: " + WorkingAddress);
        }

        //calling new on method actually hides it
        //method overhidding
        public new void DisplaySameInformation()
        {
            base.DisplayInformation();
            Console.WriteLine("Employee working address: " + WorkingAddress);
        }
    }

    #region Excercise 19 Slide Inheritance
    class Publication
    {
        protected string Title;
        protected float Price;

        public Publication() { }

        public virtual void GetData()
        {
            Console.WriteLine("Enter title:");
            Title = Console.ReadLine();
            Console.WriteLine("Enter price:");
            Price = (float)(Convert.ToDouble(Console.ReadLine()));
        }

        public virtual void PutData()
        {
            Console.WriteLine("Title:" + Title);
            Console.WriteLine("Price:" + Price);
        }
    }

    /**
     * Book class
     */
    class Book : Publication
    {
        int PageCount;

        public Book(): base() { }

        public override void GetData()
        {
            base.GetData();
            Console.WriteLine("Enter page count: ");
            PageCount = Convert.ToInt32(Console.ReadLine());
        }

        public override void PutData()
        {
            base.PutData();
            Console.WriteLine("Page count: " + PageCount);
        }
    }

    /**
     * CD class
     */
    class CD : Publication
    {
        int PlayingTime;

        public CD() : base() { }

        public override void GetData()
        {
            base.GetData();
            Console.WriteLine("Enter playing time: ");
            PlayingTime = Convert.ToInt32(Console.ReadLine());
        }

        public override void PutData()
        {
            base.PutData();
            Console.WriteLine("Playing time: " + PlayingTime);
        }
    }
    #endregion

    class Program
    {
        static void Main(string[] args)
        {
            Employee e = new Employee(1, "camnh", 30, "hanoi university");
            e.DisplayInformation();
            
            Person e1 = new Employee(2, "thangnx", 40, "hanoi university");
            //print information of child class (Employee class)
            e1.DisplayInformation();
            //new was called, using method FROM parent Class (Person class).
            e1.DisplaySameInformation();
          
            #region main() from Slide 19 above
            Book book = new Book();
            book.GetData();
            book.PutData();

            CD cd = new CD();
            cd.GetData();
            cd.PutData();
            #endregion

            Console.ReadLine();
        }
    }
}
