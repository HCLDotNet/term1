﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EnumAndStruct
{
    struct Box
    {
        public int Width;
        public int Height;

        public Box(int width, int height)
        {
            Width = width;
            Height = height;
        }
    }

    /*
     * Exercise Slide 60 - Classes and Objects
     */
     struct StringFromDate
     {
        public string Year; 
        public int Month;
        public string Day;

        public StringFromDate(string day, int month, string year)
        {
            Year = year;
            Month = month;
            Day = day;
        }

        public void Display()
        {           
            Console.WriteLine("{0}", this.Day);
        }
     }

    class Program
    {
        enum DaysOfWeek
        {
            Monday,
            Tuesday,
            Wednesday
        }

        enum MonthOfDate
        {
            January = 1,
            February = 2,
            March = 3,
            April = 4,
            May = 5,
            June = 6,
            July = 7,
            August = 8,
            September = 9,
            October = 10,
            November = 11,
            December = 12,
        }

        static void Main(string[] args)
        {
            Console.WriteLine(Program.DaysOfWeek.Monday); //print Monday

            Console.WriteLine((int)Program.DaysOfWeek.Wednesday); //print 2 -- index

            //using struct
            Box box = new Box(5,6);
            Console.WriteLine("Width: {0}, height: {1}", box.Width, box.Height);
            //assign struct property value
            box.Width = 15;
            Console.WriteLine("Width: {0}", box.Width); //Write width


            #region Main Of Struct StringOfDate
            StringFromDate stringFromDate = new StringFromDate("7", (int)MonthOfDate.July, "2020");
            stringFromDate.Display();
            #endregion
            Console.ReadLine();
        }
    }
}
