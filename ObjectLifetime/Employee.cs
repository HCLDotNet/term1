﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectLifetime
{
    class Person
    {
        ~Person()
        {
            Console.WriteLine("Person ctor called");
        }
    }
    class Employee : Person
    {
        ~Employee()
        {
            Console.WriteLine("Employee constructor called");
        }
    }

    class Manager : Employee
    {
        ~Manager()
        {
            Console.WriteLine("Manager constructor called");
        }
    }
}

