﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ObjectLifetime
{
    class Customer : IDisposable
    {
        public int id;
        public string name;
        public Customer(int id, string n)
        {
            this.id = id;
            this.name = n;
        }
        public void Dispose()
        {
            Console.WriteLine("Disposed");
        }
        ~Customer()
        {
            Console.WriteLine("Destructor called");
        }
    }
}
