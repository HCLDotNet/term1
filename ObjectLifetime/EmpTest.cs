﻿using System;
namespace ObjectLifetime
{
    public class EmpTest
    {
        int id;
        string name;

        public EmpTest()
        {

        }

        public EmpTest(int id,string name)
        {
            this.id = id;
            this.name = name;
        }

        //

        ~EmpTest()
        {
            //clean up here.
            Console.WriteLine("SMT");

            //put code here to free memory

            // ==>

            //base class
            //try {
            // Console.Writeline("SMT");
            //} finally {
            //  base.Finalize(this);
            //}
        }
    }
}
