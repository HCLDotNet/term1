﻿using System;
namespace ObjectLifetime
{
    //let the class implements IDisposable to implement Dispose()

    public class DisposableClass : IDisposable
    {
        public void Dispose()
        {
            //clean up code// freeing File, SQLCOnnection, Network
            //free resource
            throw new NotImplementedException();
        }
    }

    public class DisposableRunner
    {
        public static void Run()
        {
            DisposableClass disposableClass = new DisposableClass();
            //Dispose(): resource is freed immediately, do not need GC to free memory for you
            disposableClass.Dispose();

            GC.Collect();
            Console.ReadLine();
            //Finalize() == Destructor //put queue
        }
    }
}
