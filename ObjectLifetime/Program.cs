﻿using System;

namespace ObjectLifetime
{
    class Program
    {
        static void Main(string[] args)
        {

            DisposePattern disposePattern = new DisposePattern();
            Console.WriteLine("Disposable is created...");
            disposePattern = null;
            Console.WriteLine("Assigned null... Object is destructuring");
            Console.ReadLine();
        }
    }
}
