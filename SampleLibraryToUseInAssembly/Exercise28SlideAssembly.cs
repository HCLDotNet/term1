﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SampleLibrary
{
    //in order to import, public must be used for class
    public class Exercise28SlideAssembly
    {
        public static string ConvertRoman(int number)
        {
            if ((number < 0) || (number > 3999)) throw new ArgumentOutOfRangeException("insert value betwheen 1 and 3999");
            if (number < 1) return string.Empty;
            if (number >= 1000) return "M" + ConvertRoman(number - 1000);
            if (number >= 900) return "CM" + ConvertRoman(number - 900);
            if (number >= 500) return "D" + ConvertRoman(number - 500);
            if (number >= 400) return "CD" + ConvertRoman(number - 400);
            if (number >= 100) return "C" + ConvertRoman(number - 100);
            if (number >= 90) return "XC" + ConvertRoman(number - 90);
            if (number >= 50) return "L" + ConvertRoman(number - 50);
            if (number >= 40) return "XL" + ConvertRoman(number - 40);
            if (number >= 10) return "X" + ConvertRoman(number - 10);
            if (number >= 9) return "IX" + ConvertRoman(number - 9);
            if (number >= 5) return "V" + ConvertRoman(number - 5);
            if (number >= 4) return "IV" + ConvertRoman(number - 4);
            if (number >= 1) return "I" + ConvertRoman(number - 1);
            throw new ArgumentOutOfRangeException("something bad happened");
        }

        public static string ConvertWords(int number)
        {
            if (number == 0)
                return "zero";

            if (number < 0)
                return "minus " + ConvertWords(Math.Abs(number));

            string words = "";

            if ((number / 1000000) > 0)
            {
                words += ConvertWords(number / 1000000) + " million ";
                number %= 1000000;
            }

            if ((number / 1000) > 0)
            {
                words += ConvertWords(number / 1000) + " thousand ";
                number %= 1000;
            }

            if ((number / 100) > 0)
            {
                words += ConvertWords(number / 100) + " hundred ";
                number %= 100;
            }

            if (number > 0)
            {
                if (words != "")
                    words += "and ";

                var unitsMap = new[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
                var tensMap = new[] { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

                if (number < 20)
                    words += unitsMap[number];
                else
                {
                    words += tensMap[number / 10];
                    if ((number % 10) > 0)
                        words += "-" + unitsMap[number % 10];
                }
            }

            return words;
        }
    }
}
