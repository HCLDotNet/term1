﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Term1
{
    class Program
    {
        static void Main(string[] args)
        {
            double a = 43.12;
            Console.WriteLine("{0:0.00}", a);
            //Print something to the screen
            Console.WriteLine("Hello world");
            //Get user input key to terminate program
            Console.ReadLine();
        }
    }

    //use for assembly lesson
    class Sample
    {
        public static void PrintInfo()
        {
            Console.WriteLine("Hello world");
        }
    }
}
