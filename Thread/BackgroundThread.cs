﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadLesson
{
    class BackgroundThread
    {
        public static void Run()
        {
            Thread t1 = new Thread(new ThreadStart(ForegroundThreadStart));            
            t1.Start();

            //When set thread to background, if any foreground threads run
            //background thread is still running. However, if foreground thread stop
            //execution, all background threads stop functioning.
            Thread t2 = new Thread(new ThreadStart(BackgroundThreadStart));    
            //A thread is marked as background set IsBackground to true.
            t2.IsBackground = true;
            t2.Start();

            //Main thread exits; however, 2 threads are still running
            Console.WriteLine("Thread ending");
        }

        public static void ForegroundThreadStart()
        {
            //Foreground threads run 4 times, when exit, all background threads
            //which is currently running exits
            for (int i = 0; i < 20; i++)
            {             
                Console.WriteLine("Foreground Thread stared {0}", i);                
                Thread.Sleep(1000);
            }            
        }

        public static void BackgroundThreadStart()
        {
            for (int i = 0; i < 8000; i++)
            {
                Console.WriteLine("Background Thread stared {0}", i);
                Thread.Sleep(1000);
            }
        }       
    }
}
