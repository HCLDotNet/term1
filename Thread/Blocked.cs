﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadLesson
{
    class Blocked
    {
        static int count = 0;
        static int cd = 0;
        public static void Run()
        {
            Console.WriteLine("123");
            Thread t = new Thread(new ThreadStart(AnswerQA));
            Thread b = new Thread(new ThreadStart(DisplayCountdown));
            t.Start();
            b.Start();
            b.Join();


            Console.WriteLine(count);
            Console.ReadLine();
        }

        public static void DisplayCountdown()
        {
            try
            {
                for (int i = 5; i >= 0; --i)
                {
                    Console.Write("Time: {0}", i);
                    cd = i;
                    Thread.Sleep(1000);
                }
            } catch (Exception e)
            {
                Console.WriteLine("Countdown aborted");
            }
        }

        public static void AnswerQA()
        {
            try
            {
                Console.WriteLine("Answer A");
                string answerA = Console.ReadLine();
                if (cd == 0)
                {

                }
                else
                {
                    if (answerA == "A")
                    {
                        count++;
                    }
                }
                
            } catch (ThreadAbortException e)
            {
                Console.WriteLine("Aborted");
            }
        }
    }
}
