﻿using System;
using System.Threading;

namespace ThreadLesson
{
    public class InterruptThread
    {
        public static void Print()
        {
            try
            {
                for (int i = 0; i < 10; i++)
                {
                    Console.WriteLine("ABC {0}", i);
                    Thread.Sleep(1000);
                }
            } catch (Exception e)
            {
                Console.WriteLine("Interrupted");
            }
            
        }

        public static void AnotherPrint()
        {
            for (int i = 0; i < 10000; i++)
            {
                Console.WriteLine("Bg print {0}", i);
                Thread.Sleep(1000);
            }
        }

        public static void Run()
        {
            Thread t = new Thread(new ThreadStart(Print));
            t.Start();
            Thread t2 = new Thread(new ThreadStart(AnotherPrint));
            t2.IsBackground = true;
            t2.Start();

            Console.WriteLine("Press any key to stop");
            int i = Console.Read();
            if (i != null)
            {
                t.Interrupt();
            }
            
            Console.WriteLine("Interrupted Exiting");

            Console.WriteLine("Running");
            
        }
    }
}
