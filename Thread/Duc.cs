﻿using System;
using System.Threading;
using System.Timers;



class Duc
{
    public static int count = 0;
    //public static string input;
    //private static System.Timers.Timer aTimer;
    //private static void CounterTimeElapse()
    //{
    //    aTimer = new System.Timers.Timer(5000);
    //    aTimer.Elapsed += OnTimedEvent;
    //    aTimer.Interval = 1000;
    //    aTimer.AutoReset = true;
    //    aTimer.Enabled = true;
    //    aTimer.Start();
    //}
    //private static void OnTimedEvent(Object source, ElapsedEventArgs e)
    //{
    //    input = Console.ReadLine();
    //}
    public static void questionA()
    {
        try
        {
            Console.WriteLine("Enter A to get correct answer:");

            string input = Console.ReadLine();
            if (input == "A")
            {
                count++;
            }
        }
        catch (ThreadAbortException e)
        {
            Console.WriteLine("A aborted");
        }
        catch (ThreadInterruptedException e)
        {
            Console.WriteLine("A interrupted");
        }

    }
    public static void questionC()
    {
        try
        {
            Console.WriteLine("Enter C to get correct answer:");

            string input = Console.ReadLine();

            if (input == "C")
                count++;
        }
        catch (ThreadAbortException e)
        {
            Console.WriteLine("C aborted");
        }

    }
    public static void questionB()
    {
        try
        {
            Console.WriteLine("Enter B to get correct answer:");

            string input = Console.ReadLine();

            if (input == "B")
            {
                count++;
            }
        }
        catch (ThreadAbortException e)
        {
            Console.WriteLine("B aborted");
        }
    }



    public static void Run()
    {
        Thread a = new Thread(new ThreadStart(questionA));
        a.Name = "a";
        a.Start();
        Thread.Sleep(5000);
        
        if (a.IsAlive)
        {
            a.Abort();
        }

        Thread b = new Thread(new ThreadStart(questionB));
        b.Name = "b";
        b.Start();
        Thread.Sleep(5000);
        if (b.IsAlive)
        {
            b.Abort();
        }

        Thread c = new Thread(new ThreadStart(questionC));
        c.Name = "c";
        c.Start();
        Thread.Sleep(5000);

        if (c.IsAlive)
        {
            c.Abort();
        }

        Thread.Sleep(3000);
        Console.WriteLine("Count " + count);
        Console.ReadLine();
    }




}