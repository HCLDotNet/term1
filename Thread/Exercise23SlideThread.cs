﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace ThreadLesson
{
    public class Exercise23SlideThread
    {
        static int mark = 0;

        public static void Run()
        {
            Thread tA = new Thread(new ThreadStart(ShowQuestion1));
            tA.Start();
            //delay for 5 sec, put Thread into sleep, after 5 sec, abort the thread and 
            //move to the next thread for the next question
            Thread.Sleep(5000);

            //if (tA.IsAlive)
            //{
            //    //kill thread, move to the next thread
            //    tA.Abort();
            //}

            Thread tB = new Thread(new ThreadStart(ShowQuestion2));
            tB.Start();
            Thread.Sleep(5000);
            if (tB.IsAlive)
            {
                tB.Abort();
            }

            Thread tC = new Thread(new ThreadStart(ShowQuestion3));
            tC.Start();
            Thread.Sleep(5000);
            if (tC.IsAlive)
            {
                tC.Abort();
            }

            Console.WriteLine("Final mark: {0}", mark);
            Console.ReadLine();
        }

        public static void ShowQuestion1()
        {
            Console.WriteLine("Question A: Type exact A to complete");
            string answerA = Console.ReadLine();            

            if (answerA == "A")
            {
                mark++;
            }                
            
        }

        public static void ShowQuestion2()
        {
            Console.WriteLine("Question B: Type exact B to complete");
            string answerB = Console.ReadLine();
            if (answerB == "B")
            {
                mark++;
            }
        }

        public static void ShowQuestion3()
        {
            Console.WriteLine("Question C: Type exact C to complete");
            string answerC = Console.ReadLine();

            if (answerC == "C")
            {
                mark++;
            }
        }
    }
}
