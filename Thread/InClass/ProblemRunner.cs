﻿using System;
namespace ThreadLesson.InClass
{
    public class ProblemRunner
    {
        public static void Print()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("NUmber is {0}", i);
            }
        }

        public static void Run()
        {
            //main thread

            Print();    //execute first
            Print();    //execute lateron after first completes
            Print();    //execute lateron after second completes
        }
    }
}
