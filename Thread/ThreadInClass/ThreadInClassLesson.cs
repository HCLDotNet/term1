﻿using System;
using System.Threading;

namespace ThreadLesson.ThreadInClass
{
    public class ThreadInClassLesson
    {
        public static void Print()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Thread {0}, Number is {1}", Thread.CurrentThread.Name, i);
                Thread.Sleep(1000);//1000 = 1sec
            }
        }

        public static void Run()
        {
            Thread t = new Thread(new ThreadStart(Print));
            Thread t1 = new Thread(new ThreadStart(Print));
            t.Start();           
            t1.Start();

            t.Abort();

            Console.WriteLine("Execute me");

            Console.ReadLine();

            //create a thread but at this point you do not start it
        }
    }
}
