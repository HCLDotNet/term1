﻿using System;
using System.Threading;

namespace ThreadLesson.ThreadInClass
{
    public class InClassParameterizedThreadStartLesson
    {
        public static void PrintWithParam(object obj)
        {
            try
            {
                string input = (string)obj;
                for (int i = 0; i < 10000; i++)
                {
                    Console.WriteLine("Input is{0}---Thread {1} ----Number is {2}", input, Thread.CurrentThread.Name, i);
                    Thread.Sleep(1000);//Thread is put in Wait sleep join mode
                }
            }
            catch (ThreadInterruptedException e)
            {
                Console.WriteLine("Thread interrupted");
            }
            catch (ThreadAbortException e)
            {
                Console.WriteLine("Thread aborted");
            }
        }



        public static void PrintWithParam2(object obj)
        {
            string input = (string)obj;
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine("Input is {0}---Thread {1} ----Number is {2}", input, Thread.CurrentThread.Name, i);
                Thread.Sleep(1000);
            }
        }
        public static void Run()
        {
            //Main thread
            Thread t = new Thread(new ParameterizedThreadStart(PrintWithParam)); //unstarted
            Thread t1 = new Thread(new ParameterizedThreadStart(PrintWithParam2)); //unstarted
            t.Name = "t";
            t1.Name = "t1";
            t1.IsBackground = true;
            t.Start(" abc "); //ready state and running state
            t1.Start(" bbb "); //t1 is ready and running
            Console.WriteLine("Enter choice");
            string choice = Console.ReadLine();
            if (choice == "a")
            {
                //stop the thread immediately
                //Interrupt runs when thread is in WLJ state
                //Abort stops thread even if it's not in WLJ state.
                t.Abort();
            }
            if (choice == "i")
            {
                //Only trigger when the thread is in waitsleepJoin state.
                //if thread is not in this state => Interrupt is not called.
                t.Interrupt();
            }
            Console.WriteLine("Execute me");
            Console.ReadLine();
        }
    }
}
