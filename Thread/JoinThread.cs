﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadLesson
{
    class JoinThread
    {
        int ind = 0;
        public void CountUp()
        {
            while (ind < 10)
            {
                ind++;
                Console.WriteLine(Thread.CurrentThread.Name + " " +ind);
                //wait 1s before next execution
                Thread.Sleep(1000);
            }
        }

        public void PrintThread()
        {
            Console.WriteLine("Print smt");
        }


        public static void Run()
        {
            JoinThread ex = new JoinThread();
            Thread t2 = new Thread(new ThreadStart(ex.CountUp));
            t2.Name = "t2";

            Thread t3 = new Thread(new ThreadStart(ex.PrintThread));
            t3.Name = "t3";

            //trigger start() event: Change from Unstarted to Ready (Slide 7 of Thread)
            t2.Start();
            //Using Join() to wait until given thread terminate
            //Here, t2 print from 1->10
            t2.Join();
            //Print smt to console
            t3.Start();
        }
    }
    
}
