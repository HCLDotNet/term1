﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadLesson
{
    class SimpleThread
    {
        int ind = 0;
        public void CountUp()
        {
            while (ind < 10)
            {
                ind++;
                Console.WriteLine(Thread.CurrentThread.Name + " " +ind);
                Thread.Sleep(1000);
            }
        }

        public static void Run()
        {
            SimpleThread ex = new SimpleThread();
            Thread t2 = new Thread(new ThreadStart(ex.CountUp));
            t2.Name = "t2";

            Thread t3 = new Thread(new ThreadStart(ex.CountUp));
            t3.Name = "t3";

            //trigger start() event: Change from Unstarted to Ready (Slide 7 of Thread)
            t2.Start();
            t3.Start();

            Console.WriteLine("End");
        }
    }
    
}
