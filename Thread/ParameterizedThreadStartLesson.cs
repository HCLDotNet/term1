﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadLesson
{
    class ParameterizedThreadStartLesson
    {
        private static void PassParam(object o)
        {
            Console.WriteLine("Param: {0}", o);
        }

        public static void Run()
        {
            //Using ParameterizedThreadStart: Can accept params on thread
            //ThreadStart doesn't accept params
            Thread t1 = new Thread(new ParameterizedThreadStart(PassParam));
            t1.Start("Test");
        }
    }
}
