﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreadLesson
{
    class ThreadQueue
    {
        public static void PrintWithParam(object obj)
        {
            string input = (string)obj;
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Input is{0}---Thread {1} ----Number is {2}", input, Thread.CurrentThread.Name, i);
                Thread.Sleep(1000);
            }
        }

        public static void PrintWithParam2(object obj)
        {
            string input = (string)obj;
            for (int i = 0; i < 100; i++)
            {
                Console.WriteLine("Input is {0}---Thread {1} ----Number is {2}", input, Thread.CurrentThread.Name, i);
                Thread.Sleep(1000);
            }
        }
        public static void Run()
        {
            Thread t = new Thread(new ParameterizedThreadStart(PrintWithParam));
            Thread t1 = new Thread(new ParameterizedThreadStart(PrintWithParam2));
            t.Name = "t";
            t1.Name = "t1";
            t1.IsBackground = true;
            t.Start(" abc ");
            t1.Start(" bbb ");

            Console.WriteLine("Execute me");
        }
    }
}
