﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ThreadLesson.InClass;
using ThreadLesson.ThreadInClass;

namespace ThreadLesson
{
    class Program
    {
        static void Main(string[] args)
        {
            //uncomment this to enable normal thread Run
            //SimpleThread.Run();

            //uncomment this to enable parameterized ThreadRun
            //ParameterizedThreadStartLesson.Run();

            //uncomment this to enable Join Thread
            //JoinThread.Run();

            //uncomment to enable Background Thread
            //BackgroundThread.Run();

            //uncomment to enable Exercise
            //Exercise23SlideThread.Run();

            // MyThreadStart.MySampleThread.Run();

            //InterruptThread.Run();

            //ProblemRunner.Run();

            //ThreadInClassLesson.Run();
            //InClassParameterizedThreadStartLesson.Run();

            //xong.
            //Duc.Run();

            Blocked.Run();
        }
    }
}
